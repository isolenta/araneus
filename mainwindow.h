// ----------------------------------------------------------------------------
#ifndef MAINWINDOW_H
#define MAINWINDOW_H
// ----------------------------------------------------------------------------
#include <QMainWindow>
#include <QStringList>
#include <QMap>
#include <QVariant>
#include <QList>

#include <QAxObject>

#include "variablesdialog.h"
#include "managemodules.h"
// ----------------------------------------------------------------------------
#define UNUSED(x) (void)(x)
// ----------------------------------------------------------------------------
namespace Ui {
class MainWindow;
}
// ----------------------------------------------------------------------------
class Module {
public:
    QString name;
    double width;     // mm
    double height;    // mm
    double off_price;
    double min_price;
    double pixel_step;
    unsigned pixel_type;
        #define PIXEL_DEFAULT 0
        #define PIXEL_INTERP  1
    unsigned led_type;
        #define LED_DIP 0
        #define LED_SMD 1
    double matrix_width;
    double matrix_height;
    double power_avg;
    double power_max;
    double brightness;
    double weight;    
    QString led_model;

    bool operator ==(Module);
};
// ----------------------------------------------------------------------------
class Variable {
public:
    QVariant value;
    QString desc;
};
// ----------------------------------------------------------------------------
QDataStream &operator<<(QDataStream &out, const Variable &var);
QDataStream &operator>>(QDataStream &in, Variable &var);
// ----------------------------------------------------------------------------
QDataStream &operator<<(QDataStream &out, const Module &var);
QDataStream &operator>>(QDataStream &in, Module &var);
// ----------------------------------------------------------------------------
typedef QMap<QString, Variable> VariablesMap;
typedef QList<Module> ModuleStorage;
// ----------------------------------------------------------------------------
class VariablesDialog;
// ----------------------------------------------------------------------------
class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    VariablesMap variables;
    ModuleStorage modules;
    QStringList ledModels;

    void variablesReadConfig(QString cfgFilename);
    void variablesWriteConfig(QString cfgFilename);

    void modulesWriteConfig(QString cfgFilename);
    void modulesReadConfig(QString cfgFilename);

    void ledModelsReadConfig(QString cfgFilename);
    void ledModelsWriteConfig(QString cfgFilename);

    void updateMainFrame();
    void updateModParams(bool);
    void calculateParams(bool calcMetalSum = true, bool calcMetalWeight = true, bool calcNumModules = true);
    void populateScreenDimensions();
    int getDeliveryPrice();
    void closeEvent(QCloseEvent *event);

    void wordSearchReplace(QString oldString, QString newString);
    void renderToWord(QString filename, int fmt);

    void updatePriceLabels();
    void onVariablesUpdated();

private slots:
    void on_btnSaveDoc_clicked();
    void on_btnPrint_clicked();
    void on_btnManageModules_clicked();
    void on_btnVariablesEdit_clicked();
    void on_cbModules_currentIndexChanged(int index);
    void on_cbLedType_currentIndexChanged(int index);
    void on_leWidth_textChanged(const QString &arg1);
    void on_leHeight_textChanged(const QString &arg1);
    void on_lePixelStep_textChanged(const QString &arg1);
    void on_leMatrixWidth_textChanged(const QString &arg1);
    void on_leMatrixHeight_textChanged(const QString &arg1);
    void on_lePowerAvg_textChanged(const QString &arg1);
    void on_lePowerMax_textChanged(const QString &arg1);
    void on_leBrightness_textChanged(const QString &arg1);
    void on_leWeight_textChanged(const QString &arg1);
    void on_cbPixelType_currentIndexChanged(int index);
    void on_chkUseOwnPrice_clicked(bool checked);
    void on_btnSaveModule_clicked();
    void on_cbScrWidth_currentIndexChanged(int index);
    void on_cbScrHeight_currentIndexChanged(int index);
    void on_leMetalPrice_textChanged(const QString &arg1);
    void on_leVideoPrice_textChanged(const QString &arg1);
    void on_cbModPrice_currentIndexChanged(int index);
    void on_leOwnPrice_textChanged(const QString &arg1);
    void on_cbDeliveryPrice_currentIndexChanged(int index);
    void on_leMetalSum_textChanged(const QString &arg1);
    void on_cbLedModel_textChanged(const QString &arg1);
    void on_leMetalWeight_textChanged(const QString &arg1);
    void on_leNumModulesX_editingFinished();
    void on_leNumModulesY_editingFinished();
    void on_btnImage_clicked();

private:
    Ui::MainWindow *ui;
    VariablesDialog *vdialog;
    ManageModules *manageModules;
    QDoubleValidator *doubleInput;

    double scrWidth, scrHeight;
    double numModulesW, numModulesH, numModules;
    int resX, resY;
    double totalPrice, screenPrice;
    QString scrTitle;
    QString scrTitleFile;
    QString modName;
    QString exportDir;
    QAxObject *wordApp;
};
// ----------------------------------------------------------------------------
#endif // MAINWINDOW_H
// ----------------------------------------------------------------------------
// EOF
