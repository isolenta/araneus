// ----------------------------------------------------------------------------
#include <QFileDialog>
#include <QPrintDialog>
#include <QMenu>
#include <QDoubleValidator>

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "managemodules.h"
#include "renderarea.h"

#include <QTextStream>
#include <QMessageBox>
#include <QSettings>
#include <QAxWidget>

#include <math.h>

#define WORD_FORMAT_DOC     (0)
#define WORD_FORMAT_DOCX    (16)
#define WORD_FORMAT_PDF     (17)
#define WORD_PRINTING       (-1)
#define WORD_PRINT_DIALOG   (88)
// ----------------------------------------------------------------------------
double round(double d)
{
  return floor(d + 0.5);
}
// ----------------------------------------------------------------------------
// TODO: move outside
QDataStream &operator<<(QDataStream &out, const Variable &var)
{
    out << var.value << var.desc;
    return out;
}
// ----------------------------------------------------------------------------
// TODO: move outside
QDataStream &operator>>(QDataStream &in, Variable &var)
{
    in >> var.value >> var.desc;
    return in;
}
// ----------------------------------------------------------------------------
void MainWindow::ledModelsReadConfig(QString cfgFilename)
{
    QFile file(cfgFilename);

    if (file.exists())
    {
        ledModels.clear();

        file.open(QIODevice::ReadOnly);
        QDataStream in (&file);
        in >> ledModels;
    }
    else
    {
        ledModels.clear();
        ledModels.append("Nichia");
        ledModels.append("Cree");
        ledModels.append("Nationstar");
        ledModels.append("Everlight");
    }
}
// ----------------------------------------------------------------------------
void MainWindow::updatePriceLabels()
{
    // set value sign (for example '$') after label

    ui->label_20->setText( tr("Price") + QString(" (%1)").arg( variables["value"].value.toString()));
    ui->label_21->setText( tr("Metal price") + QString(" (%1)").arg( variables["value"].value.toString()));
    ui->label_22->setText( tr("Video price") + QString(" (%1)").arg( variables["value"].value.toString()));
    ui->label_23->setText( tr("Delivery price") + QString(" (%1)").arg( variables["value"].value.toString()));
    ui->label_24->setText( tr("Total price") + QString(" (%1)").arg( variables["value"].value.toString()));
    ui->label_29->setText( tr("Metal summa") + QString(" (%1)").arg( variables["value"].value.toString()));
    ui->label_36->setText( tr("Screen sum") + QString(" (%1)").arg( variables["value"].value.toString()));


    // update labels in module editor
    manageModules->updatePriceLabels(variables["value"].value.toString());
}
// ----------------------------------------------------------------------------
void MainWindow::ledModelsWriteConfig(QString cfgFilename)
{
    QFile file(cfgFilename);

    file.open(QIODevice::WriteOnly);
    QDataStream out (&file);
    out << ledModels;
}
// ----------------------------------------------------------------------------
void MainWindow::variablesReadConfig(QString cfgFilename)
{
    QFile file(cfgFilename);
    Variable var;

    if (file.exists())
    {
        // deserialize variables from file
        file.open(QIODevice::ReadOnly);
        QDataStream in (&file);
        in >> this->variables;

        // set default values for missed variables
        if (variables.contains("metal_price") == false)
        {
            var.value = QVariant(200); var.desc = tr("Metal framework price");
            variables["metal_price"] = var;
        }

        if (variables.contains("metal_weight") == false)
        {
            var.value = QVariant(5); var.desc = tr("Metal framework weight per module");
            variables["metal_weight"] = var;
        }

        if (variables.contains("module_weight") == false)
        {
            var.value = QVariant(27); var.desc = tr("Module default weight");
            variables["module_weight"] = var;
        }

        if (variables.contains("video_price") == false)
        {
            var.value = QVariant(2500); var.desc = tr("Videoserver price");
            variables["video_price"] = var;
        }

        if (variables.contains("install_price") == false)
        {
            var.value = QVariant(5000); var.desc = tr("Delivery and installation price");
            variables["install_price"] = var;
        }

        if (variables.contains("install_text") == false)
        {
            var.value = QVariant(tr("as is")); var.desc = tr("Text for delivery&install price");
            variables["install_text"] = var;
        }

        if (variables.contains("pxstep_desc") == false)
        {
            var.value = QVariant(tr("(interpolated)")); var.desc = tr("pixel step description");
            variables["pxstep_desc"] = var;
        }

        if (variables.contains("interp_desc") == false)
        {
            var.value = QVariant(tr("* The technology uses interpolation image forming LEDs included in the physical structure of the neighboring pixels. This allows you to get a better and clearer picture.")); var.desc = tr("interpolation description");
            variables["interp_desc"] = var;
        }

        if (variables.contains("angles_text_dip") == false)
        {
            var.value = QVariant(tr("View angles (by half power, h/w), degrees")); var.desc = tr("text for DIP view angles");
            variables["angles_text_dip"] = var;
        }

        if (variables.contains("angles_value_dip") == false)
        {
            var.value = QVariant(tr("120/90")); var.desc = tr("value for DIP view angles");
            variables["angles_value_dip"] = var;
        }

        if (variables.contains("angles_text_smd") == false)
        {
            var.value = QVariant(tr("View angles (by half power), degrees")); var.desc = tr("text for SMD view angles");
            variables["angles_text_smd"] = var;
        }

        if (variables.contains("angles_value_smd") == false)
        {
            var.value = QVariant(tr("120/120")); var.desc = tr("value for SMD view angles");
            variables["angles_value_smd"] = var;
        }

        if (variables.contains("value") == false)
        {
            var.value = QVariant(QString("$")); var.desc = tr("value char");
            variables["value"] = var;
        }

        if (variables.contains("show_overlay") == false)
        {
            var.value = QVariant(1); var.desc = tr("overlab drawing enable on screen image");
            variables["show_overlay"] = var;
        }

        if (variables.contains("geometry_value_dip") == false)
        {
            var.value = QVariant(tr("flask LEDs 5 pcs")); var.desc = tr("geometry value for DIP leds");
            variables["geometry_value_dip"] = var;
        }

        if (variables.contains("geometry_value_smd") == false)
        {
            var.value = QVariant(tr("SMD (3 in 1)")); var.desc = tr("geometry value for SMD leds");
            variables["geometry_value_smd"] = var;
        }
    } // file.exists()
    else
    {
        var.value = QVariant(200); var.desc = tr("Metal framework price");
        variables["metal_price"] = var;

        var.value = QVariant(5); var.desc = tr("Metal framework weight per module");
        variables["metal_weight"] = var;

        var.value = QVariant(27); var.desc = tr("Module default weight");
        variables["module_weight"] = var;

        var.value = QVariant(2500); var.desc = tr("Videoserver price");
        variables["video_price"] = var;

        var.value = QVariant(5000); var.desc = tr("Delivery and installation price");
        variables["install_price"] = var;

        var.value = QVariant(tr("as is")); var.desc = tr("Text for delivery&install price");
        variables["install_text"] = var;

        var.value = QVariant(tr("(interpolated)")); var.desc = tr("pixel step description");
        variables["pxstep_desc"] = var;

        var.value = QVariant(tr("* The technology uses interpolation image forming LEDs included in the physical structure of the neighboring pixels. This allows you to get a better and clearer picture.")); var.desc = tr("interpolation description");
        variables["interp_desc"] = var;

        var.value = QVariant(tr("View angles (by half power, h/w), degrees")); var.desc = tr("text for DIP view angles");
        variables["angles_text_dip"] = var;

        var.value = QVariant(tr("120/90")); var.desc = tr("value for DIP view angles");
        variables["angles_value_dip"] = var;

        var.value = QVariant(tr("View angles (by half power), degrees")); var.desc = tr("text for SMD view angles");
        variables["angles_text_smd"] = var;

        var.value = QVariant(tr("120/120")); var.desc = tr("value for SMD view angles");
        variables["angles_value_smd"] = var;

        var.value = QVariant(QString("$")); var.desc = tr("value char");
        variables["value"] = var;

        var.value = QVariant(1); var.desc = tr("overlab drawing enable on screen image");
        variables["show_overlay"] = var;

        var.value = QVariant(tr("flask LEDs 5 pcs")); var.desc = tr("geometry value for DIP leds");
        variables["geometry_value_dip"] = var;

        var.value = QVariant(tr("SMD (3 in 1)")); var.desc = tr("geometry value for SMD leds");
        variables["geometry_value_smd"] = var;
    }
}
// ----------------------------------------------------------------------------
void MainWindow::variablesWriteConfig(QString cfgFilename)
{
    QFile file(cfgFilename);

    // serialize variables into file
    file.open(QIODevice::WriteOnly);
    QDataStream out (&file);
    out << this->variables;
}
// ----------------------------------------------------------------------------
// TODO: move outside
QDataStream &operator<<(QDataStream &out, const Module &mod)
{
    out << mod.name
        << mod.width
        << mod.height
        << mod.off_price
        << mod.min_price
        << mod.pixel_step
        << mod.pixel_type
        << mod.led_type
        << mod.matrix_width
        << mod.matrix_height
        << mod.power_avg
        << mod.power_max
        << mod.brightness
        << mod.weight;

    return out;
}
// ----------------------------------------------------------------------------
// TODO: move outside
QDataStream &operator>>(QDataStream &in, Module &mod)
{
    in  >> mod.name
        >> mod.width
        >> mod.height
        >> mod.off_price
        >> mod.min_price
        >> mod.pixel_step
        >> mod.pixel_type
        >> mod.led_type
        >> mod.matrix_width
        >> mod.matrix_height
        >> mod.power_avg
        >> mod.power_max
        >> mod.brightness
        >> mod.weight;

    return in;
}
// ----------------------------------------------------------------------------
void MainWindow::modulesReadConfig(QString cfgFilename)
{
    QFile file(cfgFilename);

    if (file.exists())
    {
        // deserialize modules from file
        file.open(QIODevice::ReadOnly);
        QDataStream in (&file);
        in >> this->modules;

        if (this->modules.count() > 0)
            return;
    }

    QMessageBox::warning(this, tr("Warning"), tr("Modules settings empty. You need to create at least one to continue work"));
}
// ----------------------------------------------------------------------------
void MainWindow::modulesWriteConfig(QString cfgFilename)
{
    QFile file(cfgFilename);

    // serialize modules into file
    file.open(QIODevice::WriteOnly);
    QDataStream out (&file);
    out << this->modules;
}
// ----------------------------------------------------------------------------
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    // config directory
    QDir dir_c(QCoreApplication::applicationDirPath() + "/config");
    if (!dir_c.exists()) {
        dir_c.mkpath(".");
    }

    // restore saved UI params
    QSettings settings(QCoreApplication::applicationDirPath() + "/config/config.ini", QSettings::IniFormat);
    restoreGeometry(settings.value("geometry").toByteArray());
    restoreState(settings.value("windowState").toByteArray());

    setMinimumSize(726, 686);
    setMaximumSize(726, 686);

    exportDir = settings.value("exportDir", QVariant("exports")).toString();
    QDir dir_e(QCoreApplication::applicationDirPath() + "/" + exportDir);
    if (!dir_e.exists()) {
        dir_e.mkpath(".");
    }

    // statusbar labels
    QLabel *lblCopy = new QLabel(0);
    lblCopy->setText("  &copy; Araneus 2013-2014");
    lblCopy->setToolTip(QString("Build date %1").arg(__DATE__));
    lblCopy->setTextFormat(Qt::RichText);

    ui->statusBar->addWidget(lblCopy, 0);

    QLabel *lblLink = new QLabel(0);
    lblLink->setText("<a href=\"http://www.araneus.ru/\">http://www.araneus.ru/</a>");
    lblLink->setTextFormat(Qt::RichText);
    lblLink->setTextInteractionFlags(Qt::TextBrowserInteraction);
    lblLink->setOpenExternalLinks(true);
    lblLink->setAlignment(Qt::AlignRight);

    ui->statusBar->addWidget(lblLink, 1);

    // setup input validators
    doubleInput = new QDoubleValidator(this);

    ui->verticalLayout_2->insertWidget(0, ui->leOwnPrice);
    ui->leOwnPrice->setGeometry( ui->cbModPrice->geometry() );

    ui->leWidth->setValidator(doubleInput);
    ui->leHeight->setValidator(doubleInput);
    ui->leOwnPrice->setValidator(doubleInput);
    ui->lePixelStep->setValidator(doubleInput);
    ui->leMatrixHeight->setValidator(doubleInput);
    ui->leMatrixWidth->setValidator(doubleInput);
    ui->lePowerAvg->setValidator(doubleInput);
    ui->lePowerMax->setValidator(doubleInput);
    ui->leBrightness->setValidator(doubleInput);
    ui->leWeight->setValidator(doubleInput);

    ui->leVideoPrice->setValidator(doubleInput);
    ui->leMetalPrice->setValidator(doubleInput);
    ui->leVideoPrice->setValidator(doubleInput);
    ui->leMetalSum->setValidator(doubleInput);

    ui->leMetalWeightOne->setValidator(doubleInput);
    ui->leMetalWeight->setValidator(doubleInput);

    ui->cbDeliveryPrice->setValidator(doubleInput);

    ui->leNumModulesX->setValidator(doubleInput);
    ui->leNumModulesY->setValidator(doubleInput);

    // style for readonly edits
    QString styleReadOnly = "QLineEdit {border: 1px solid black; background-color: #cccccc;}";
    ui->leArea->setStyleSheet(styleReadOnly);
    ui->leScreenWeight->setStyleSheet(styleReadOnly);
    ui->lePanelsWeight->setStyleSheet(styleReadOnly);
    ui->leScrPowerAvg->setStyleSheet(styleReadOnly);
    ui->leScrPowerMax->setStyleSheet(styleReadOnly);
    ui->leResolution->setStyleSheet(styleReadOnly);
    ui->leArea->setStyleSheet(styleReadOnly);
    ui->leMetalWeightOne->setStyleSheet(styleReadOnly);
    ui->leScreenSum->setStyleSheet(styleReadOnly);

    // style for other edits
    QString styleCommon = "QLineEdit {border: 1px solid black;}";
    ui->leBrightness->setStyleSheet(styleCommon);
    ui->leHeight->setStyleSheet(styleCommon);
    ui->leMatrixHeight->setStyleSheet(styleCommon);
    ui->leMatrixWidth->setStyleSheet(styleCommon);
    ui->leMetalPrice->setStyleSheet(styleCommon);
    ui->leMetalSum->setStyleSheet(styleCommon);
    ui->leNumModulesX->setStyleSheet(styleCommon);
    ui->leNumModulesY->setStyleSheet(styleCommon);

    ui->leOwnPrice->setStyleSheet(styleCommon);
    ui->lePixelStep->setStyleSheet(styleCommon);
    ui->lePowerAvg->setStyleSheet(styleCommon);
    ui->lePowerMax->setStyleSheet(styleCommon);
    ui->leVideoPrice->setStyleSheet(styleCommon);
    ui->leWeight->setStyleSheet(styleCommon);
    ui->leWidth->setStyleSheet(styleCommon);
    ui->leMetalWeight->setStyleSheet(styleCommon);

    ui->leTotalPrice->setStyleSheet("QLineEdit {border: 1px solid black; background-color: #ffffbf;}");
    ui->leNewModName->setStyleSheet("QLineEdit {border: 1px solid red;}");

    ui->leOwnPrice->setVisible(false);
    ui->cbModPrice->setVisible(true);

    // populating variables list with default values
    variablesReadConfig(QCoreApplication::applicationDirPath() + "/config/variables.dat");

    // create Variables Dialog form
    vdialog = new VariablesDialog(this);

    // populating modules list from config file
    modulesReadConfig(QCoreApplication::applicationDirPath() + "/config/modules.dat");

    // populating led models list from config file
    ledModelsReadConfig(QCoreApplication::applicationDirPath() + "/config/leds.dat");

    ui->cbLedModel->clear();
    for (int i = 0; i < ledModels.count(); i++)
        ui->cbLedModel->addItem(ledModels.at(i));

    // create Manage modules window
    manageModules = new ManageModules(this);

    // update params
    updateMainFrame();

    // populate template list
    QDir dir(QCoreApplication::applicationDirPath() + "/templates");
    if (!dir.exists()) {
        dir.mkpath(".");
    }

    // check if templates exists
    QStringList names;
    names.append("*.doc");
    names.append("*.docx");

    QStringList templateList = dir.entryList(names, QDir::Files | QDir::NoSymLinks);

    if (templateList.count() == 0)
        QMessageBox::warning(this, tr("Warning"), tr("No template files found in template directory. You should to create at least one"));

    // fill template list
    for (int i = 0; i < templateList.count(); i++)
        ui->cbTemplate->addItem(templateList.at(i));
}
// ----------------------------------------------------------------------------
void MainWindow::closeEvent(QCloseEvent *event)
{
    QSettings settings(QCoreApplication::applicationDirPath() + "/config/config.ini", QSettings::IniFormat);
    settings.setValue("geometry", saveGeometry());
    settings.setValue("windowState", saveState());

    QMainWindow::closeEvent(event);
}
// ----------------------------------------------------------------------------
MainWindow::~MainWindow()
{
    // save variables into file
    variablesWriteConfig(QCoreApplication::applicationDirPath() + "/config/variables.dat");

    // save modules list into config file
    modulesWriteConfig(QCoreApplication::applicationDirPath() + "/config/modules.dat");

    // save LED models list into config file
    ledModelsWriteConfig(QCoreApplication::applicationDirPath() + "/config/leds.dat");

    delete doubleInput;
    delete vdialog;
    delete manageModules;
    delete ui;
}
// ----------------------------------------------------------------------------
void MainWindow::on_btnSaveDoc_clicked()
{
    QString selectedFilter;

    #define FLT_DOC  QString("Microsoft Word 97-2003 (*.doc)")
    #define FLT_DOCX QString("Microsoft Word (*.docx)")
    #define FLT_PDF  QString("Portable Document (*.pdf)")

    QString fileName = QFileDialog::getSaveFileName(this,
                                                    tr("Save as doc"),
                                                    QCoreApplication::applicationDirPath() + "/" + exportDir + "/" + scrTitleFile + ".doc",
                                                    FLT_DOC + ";;" + FLT_DOCX + ";;" + FLT_PDF,
                                                        &selectedFilter);
    if (fileName != "")
    {
        if (selectedFilter == FLT_DOC)
            renderToWord(fileName, WORD_FORMAT_DOC);
        else if (selectedFilter == FLT_DOCX)
            renderToWord(fileName, WORD_FORMAT_DOCX);
        else if (selectedFilter == FLT_PDF)
            renderToWord(fileName, WORD_FORMAT_PDF);
    }
}
// ----------------------------------------------------------------------------
void MainWindow::on_btnPrint_clicked()
{
    renderToWord("", WORD_PRINTING);
}
// ----------------------------------------------------------------------------
void MainWindow::on_btnManageModules_clicked()
{
    if (manageModules)
        manageModules->show();

    updateMainFrame();
    updateModParams(false);
    populateScreenDimensions();
    calculateParams();
}
// ----------------------------------------------------------------------------
void MainWindow::on_btnVariablesEdit_clicked()
{
    vdialog->show();
}
// ----------------------------------------------------------------------------
void MainWindow::onVariablesUpdated()
{
    updateMainFrame();
    updateModParams(false);
    populateScreenDimensions();
    calculateParams();
}
// ----------------------------------------------------------------------------
// TODO: move outside
bool Module::operator ==(Module a)
{
    return (
                (this->name == a.name) &&
                (this->width == a.width) &&
                (this->height == a.height) &&
                (this->off_price == a.off_price) &&
                (this->min_price == a.min_price) &&
                (this->pixel_step == a.pixel_step) &&
                (this->pixel_type == a.pixel_type) &&
                (this->led_type == a.led_type) &&
                (this->matrix_width == a.matrix_width) &&
                (this->matrix_height == a.matrix_height) &&
                (this->power_avg == a.power_avg) &&
                (this->power_max == a.power_max) &&
                (this->brightness == a.brightness) &&
                (this->weight == a.weight)
            );
}
// ----------------------------------------------------------------------------
void MainWindow::updateMainFrame()
{
    int savedCurrentIndex = -1;

    // save selected index to restore it after repopulating list
    if (ui->cbModules->currentIndex() != -1)
        savedCurrentIndex = ui->cbModules->currentIndex();

    ui->cbModules->clear();

    // fill cbModules
    for (int i = 0; i < modules.count(); i++)
    {
        ui->cbModules->addItem(modules.at(i).name);
    }

    // restore selected item
    ui->cbModules->setCurrentIndex(savedCurrentIndex);

    // populate checkboxes
    ui->cbLedType->clear();
    ui->cbLedType->addItem(tr("DIP"));
    ui->cbLedType->addItem(tr("SMD"));

    ui->cbPixelType->clear();
    ui->cbPixelType->addItem(tr("default"));
    ui->cbPixelType->addItem(tr("interpolate"));

    // add value sign
    updatePriceLabels();
}
// ----------------------------------------------------------------------------
int MainWindow::getDeliveryPrice()
{
    if (ui->cbDeliveryPrice->currentIndex() == 0)
        return 0;
    else
        return ui->cbDeliveryPrice->itemText(ui->cbDeliveryPrice->currentIndex()    ).toInt();
}
// ----------------------------------------------------------------------------
void MainWindow::on_cbDeliveryPrice_currentIndexChanged(int index)
{
    if (index == 0)
        ui->cbDeliveryPrice->setEditable(false);
    else
        ui->cbDeliveryPrice->setEditable(true);

    calculateParams();
}
// ----------------------------------------------------------------------------
void MainWindow::on_cbModules_currentIndexChanged(int index)
{
    ui->leNewModName->setVisible(false);
    ui->btnSaveModule->setEnabled(false);

    if (index == -1)
    {
        ui->leWidth->setText("");
        ui->leHeight->setText("");
        ui->lePixelStep->setText("");
        ui->leMatrixWidth->setText("");
        ui->leMatrixHeight->setText("");
        ui->lePowerAvg->setText("");
        ui->lePowerMax->setText("");
        ui->cbPixelType->setCurrentIndex(0);
        ui->cbLedType->setCurrentIndex(0);
        ui->leBrightness->setText("");
        ui->leWeight->setText(variables["module_weight"].value.toString());

        ui->cbLedModel->clear();
        for (int i = 0; i < ledModels.count(); i++)
            ui->cbLedModel->addItem(ledModels.at(i));
    }
    else
    {
        // recalculate all params

        ui->leWidth->setText(QString("%1").arg(modules.at(index).width));
        ui->leHeight->setText(QString("%1").arg(modules.at(index).height));
        ui->lePixelStep->setText(QString("%1").arg(modules.at(index).pixel_step));
        ui->leMatrixWidth->setText(QString("%1").arg(modules.at(index).matrix_width));
        ui->leMatrixHeight->setText(QString("%1").arg(modules.at(index).matrix_height));
        ui->lePowerAvg->setText(QString("%1").arg(modules.at(index).power_avg));
        ui->lePowerMax->setText(QString("%1").arg(modules.at(index).power_max));

        if (modules.at(index).pixel_type == PIXEL_DEFAULT)
            ui->cbPixelType->setCurrentIndex(0);
        else
            ui->cbPixelType->setCurrentIndex(1);

        if (modules.at(index).led_type == LED_DIP)
        {
            ui->cbLedType->setCurrentIndex(0);
            ui->cbPixelType->setEnabled(true);
        }
        else // LED_SMD
        {
            ui->cbLedType->setCurrentIndex(1);
            ui->cbPixelType->setCurrentIndex(0); // set type to PIXEL_DEFAULT
            ui->cbPixelType->setEnabled(false);
        }

        ui->leBrightness->setText(QString("%1").arg(modules.at(index).brightness));
        ui->leWeight->setText(QString("%1").arg(modules.at(index).weight));

        ui->cbModPrice->clear();
        ui->cbModPrice->addItem(QString("%1").arg(modules.at(index).off_price) + QString(" (") + tr("off") + QString(")"));
        ui->cbModPrice->addItem(QString("%1").arg(modules.at(index).min_price) + QString(" (") + tr("min") + QString(")"));

        ui->cbLedModel->clear();
        for (int i = 0; i < ledModels.count(); i++)
            ui->cbLedModel->addItem(ledModels.at(i));

        bool lm_found = false;
        for (int i = 0; i < ui->cbLedModel->count(); i++)
        {
            if (ui->cbLedModel->itemText(i) == modules.at(index).led_model)
            {
                ui->cbLedModel->setCurrentIndex(i);
                lm_found = true;
                break;
            }
        }

        if (!lm_found && modules.at(index).led_model != "")
        {
            ui->cbLedModel->addItem(modules.at(index).led_model);
            ui->cbLedModel->setCurrentIndex( ui->cbLedModel->count() - 1 );
        }
    }

    // set default from variables
    ui->leMetalPrice->setText(variables["metal_price"].value.toString());
    ui->leVideoPrice->setText(variables["video_price"].value.toString());

    ui->cbDeliveryPrice->clear();
    ui->cbDeliveryPrice->addItem(variables["install_text"].value.toString());
    ui->cbDeliveryPrice->addItem(variables["install_price"].value.toString());

    populateScreenDimensions();
    calculateParams();
}
// ----------------------------------------------------------------------------
void MainWindow::on_cbLedType_currentIndexChanged(int index)
{
    if (index == 0)
        ui->cbPixelType->setEnabled(true);
    else
    {
        ui->cbPixelType->setCurrentIndex(0); // set to PIXEL_DEFAULT
        ui->cbPixelType->setEnabled(false);
    }

    if (ui->cbModules->currentIndex() != -1)
    {
        if (modules.at(ui->cbModules->currentIndex()).led_type != (unsigned)index)
            updateModParams(true);
        else
            updateModParams(false);
    }
    else
    {
        if (index != 0)
            updateModParams(true);
        else
            updateModParams(false);
    }

    calculateParams();
}
// ----------------------------------------------------------------------------
void MainWindow::updateModParams(bool arg)
{
    ui->leNewModName->setVisible(arg);
    ui->btnSaveModule->setEnabled(arg);
}
// ----------------------------------------------------------------------------
void MainWindow::on_leWidth_textChanged(const QString &arg1)
{
    if (ui->cbModules->currentIndex() != -1)
    {
        if (QString("%1").arg(modules.at(ui->cbModules->currentIndex()).width) != arg1)
            updateModParams(true);
        else
            updateModParams(false);
    }
    else
    {
        if (arg1 != "")
            updateModParams(true);
        else
            updateModParams(false);
    }

    populateScreenDimensions();
    calculateParams();
}
// ----------------------------------------------------------------------------
void MainWindow::on_leHeight_textChanged(const QString &arg1)
{
    if (ui->cbModules->currentIndex() != -1)
    {
        if (QString("%1").arg(modules.at(ui->cbModules->currentIndex()).height) != arg1)
            updateModParams(true);
        else
            updateModParams(false);
    }
    else
    {
        if (arg1 != "")
            updateModParams(true);
        else
            updateModParams(false);
    }

    populateScreenDimensions();
    calculateParams();
}
// ----------------------------------------------------------------------------
void MainWindow::on_lePixelStep_textChanged(const QString &arg1)
{
    if (ui->cbModules->currentIndex() != -1)
    {
        if (QString("%1").arg(modules.at(ui->cbModules->currentIndex()).pixel_step) != arg1)
            updateModParams(true);
        else
            updateModParams(false);
    }
    else
    {
        if (arg1 != "")
            updateModParams(true);
        else
            updateModParams(false);
    }

    calculateParams();
}
// ----------------------------------------------------------------------------
void MainWindow::on_leMatrixWidth_textChanged(const QString &arg1)
{
    if (ui->cbModules->currentIndex() != -1)
    {
        if (QString("%1").arg(modules.at(ui->cbModules->currentIndex()).matrix_width) != arg1)
            updateModParams(true);
        else
            updateModParams(false);
    }
    else
    {
        if (arg1 != "")
            updateModParams(true);
        else
            updateModParams(false);
    }

    calculateParams();
}
// ----------------------------------------------------------------------------
void MainWindow::on_leMatrixHeight_textChanged(const QString &arg1)
{
    if (ui->cbModules->currentIndex() != -1)
    {
        if (QString("%1").arg(modules.at(ui->cbModules->currentIndex()).matrix_height) != arg1)
            updateModParams(true);
        else
            updateModParams(false);
    }
    else
    {
        if (arg1 != "")
            updateModParams(true);
        else
            updateModParams(false);
    }

    calculateParams();
}
// ----------------------------------------------------------------------------
void MainWindow::on_lePowerAvg_textChanged(const QString &arg1)
{
    if (ui->cbModules->currentIndex() != -1)
    {
        if (QString("%1").arg(modules.at(ui->cbModules->currentIndex()).power_avg) != arg1)
            updateModParams(true);
        else
            updateModParams(false);
    }
    else
    {
        if (arg1 != "")
            updateModParams(true);
        else
            updateModParams(false);
    }

    calculateParams();
}
// ----------------------------------------------------------------------------
void MainWindow::on_lePowerMax_textChanged(const QString &arg1)
{
    if (ui->cbModules->currentIndex() != -1)
    {
        if (QString("%1").arg(modules.at(ui->cbModules->currentIndex()).power_max) != arg1)
            updateModParams(true);
        else
            updateModParams(false);
    }
    else
    {
        if (arg1 != "")
            updateModParams(true);
        else
            updateModParams(false);
    }

    calculateParams();
}
// ----------------------------------------------------------------------------
void MainWindow::on_leBrightness_textChanged(const QString &arg1)
{
    if (ui->cbModules->currentIndex() != -1)
    {
        if (QString("%1").arg(modules.at(ui->cbModules->currentIndex()).brightness) != arg1)
            updateModParams(true);
        else
            updateModParams(false);
    }
    else
    {
        if (arg1 != "")
            updateModParams(true);
        else
            updateModParams(false);
    }

    calculateParams();
}
// ----------------------------------------------------------------------------
void MainWindow::on_leWeight_textChanged(const QString &arg1)
{
    if (ui->cbModules->currentIndex() != -1)
    {
        if (QString("%1").arg(modules.at(ui->cbModules->currentIndex()).weight) != arg1)
            updateModParams(true);
        else
            updateModParams(false);
    }
    else
    {
        if (arg1 != "")
            updateModParams(true);
        else
            updateModParams(false);
    }

    calculateParams();
}
// ----------------------------------------------------------------------------
void MainWindow::on_cbPixelType_currentIndexChanged(int index)
{
    if (ui->cbModules->currentIndex() != -1)
    {
        if (modules.at(ui->cbModules->currentIndex()).pixel_type != (unsigned)index)
            updateModParams(true);
        else
            updateModParams(false);
    }
    else
    {
        if (index != 0)
            updateModParams(true);
        else
            updateModParams(false);
    }

    calculateParams();
}
// ----------------------------------------------------------------------------
void MainWindow::on_chkUseOwnPrice_clicked(bool checked)
{
    ui->leOwnPrice->setVisible(checked);
    ui->cbModPrice->setVisible(!checked);
}
// ----------------------------------------------------------------------------
void MainWindow::on_btnSaveModule_clicked()
{
    // append new module to internal list
    Module *mod = new Module;

    mod->name = ui->leNewModName->text();
    mod->width = ui->leWidth->text().toDouble();
    mod->height = ui->leHeight->text().toDouble();
    mod->off_price = ui->cbModPrice->itemText(0).toDouble();
    mod->min_price = ui->cbModPrice->itemText(1).toDouble();
    mod->pixel_step = ui->lePixelStep->text().toDouble();
    mod->pixel_type = ui->cbPixelType->currentIndex();
    mod->led_type = ui->cbLedType->currentIndex();
    mod->matrix_height = ui->leMatrixHeight->text().toDouble();
    mod->matrix_width = ui->leMatrixWidth->text().toDouble();
    mod->power_avg = ui->lePowerAvg->text().toDouble();
    mod->power_max = ui->lePowerMax->text().toDouble();
    mod->brightness = ui->leBrightness->text().toDouble();
    mod->weight = ui->leWeight->text().toDouble();

    modules.append(*mod);

    // update ledModels list
    ledModels.clear();
    for (int i = 0; i < ui->cbLedModel->count(); i++)
        ledModels.append( ui->cbLedModel->itemText(i));

    // update "save as new" button and lineedit
    updateModParams(false);

    // update modules combobox
    updateMainFrame();

    // set combobox index to last item (our new module)
    ui->cbModules->setCurrentIndex( ui->cbModules->count() - 1 );
}
// ----------------------------------------------------------------------------
void MainWindow::on_cbLedModel_textChanged(const QString &arg1)
{
    bool changed = true;

    for (int i = 0; i < ledModels.count(); i++)
        if (ledModels.at(i) == arg1)
            changed = false;

    updateModParams(changed);
}
// ----------------------------------------------------------------------------
void MainWindow::calculateParams(bool calcMetalSum,
                                 bool calcMetalWeight, bool calcNumModules)
{
    // two ways of module dimensions calculation
    // calculation from dimensions to num modules
    if (calcNumModules)
    {
        if (ui->cbScrWidth->currentIndex() < 1)
            scrWidth = 0;
        else
            scrWidth = ui->cbScrWidth->itemText(ui->cbScrWidth->currentIndex()).toDouble();

        if (ui->cbScrHeight->currentIndex() < 1)
            scrHeight = 0;
        else
            scrHeight = ui->cbScrHeight->itemText(ui->cbScrHeight->currentIndex()).toDouble();

        // calculate amount of modules
        try {
            if ((ui->leWidth->text() != "") && (ui->leWidth->text().toDouble() != 0))
                numModulesW = ( scrWidth / (ui->leWidth->text().toDouble()));
            else
                numModulesW = 0;

            if ((ui->leHeight->text() != "") && (ui->leHeight->text().toDouble() != 0))
                numModulesH = ( scrHeight / (ui->leHeight->text().toDouble()));
            else
                numModulesH = 0;

            numModules =   numModulesW * numModulesH;

            ui->leNumModulesX->setText(QString("%L1").arg(numModulesW));
            ui->leNumModulesY->setText(QString("%L1").arg(numModulesH));
        } catch(int){
            numModules = 0;
            ui->leNumModulesX->setText("0");
            ui->leNumModulesY->setText("0");
        }
    }
    // calculation from num modules to dimensions
    else
    {
        QString rawNumModulesX = ui->leNumModulesX->text();
        QString rawNumModulesY = ui->leNumModulesY->text();
        rawNumModulesX.remove(QLocale::system().groupSeparator());
        rawNumModulesY.remove(QLocale::system().groupSeparator());

        numModulesW = rawNumModulesX.toDouble();
        numModulesH = rawNumModulesY.toDouble();

        scrWidth = ui->leWidth->text().toDouble() * numModulesW;
        scrHeight = ui->leHeight->text().toDouble() * numModulesH;

        int idx;
        if (scrWidth != 0)
        {
            if ( (idx = ui->cbScrWidth->findText(QString("%1").arg(scrWidth))) != -1)
            {
                    ui->cbScrWidth->setCurrentIndex(idx);
            }
            else
            {
                // find nearest item in list
                double min = fabs(ui->cbScrWidth->itemText(0).toDouble() - scrWidth);
                int min_idx = 0;
                for (int i = 0; i < ui->cbScrWidth->count(); i++)
                {
                    if (min > ui->cbScrWidth->itemText(i).toDouble())
                    {
                        min = fabs(ui->cbScrWidth->itemText(i).toDouble() - scrWidth);
                        min_idx = i;
                    }
                }

                // insert new item in appropriate place
                ui->cbScrWidth->insertItem(min_idx+2, QString("%1").arg(scrWidth));
                ui->cbScrWidth->setCurrentIndex( min_idx+2 );
            }
        }

        if (scrHeight != 0)
        {
            if ((idx = ui->cbScrHeight->findText(QString("%1").arg(scrHeight))) != -1)

            {
                ui->cbScrHeight->setCurrentIndex(idx);
            }
            else
            {
                // find nearest item in list
                double min = fabs(ui->cbScrHeight->itemText(0).toDouble() - scrHeight);
                int min_idx = 0;
                for (int i = 0; i < ui->cbScrHeight->count(); i++)
                {
                    if (min > ui->cbScrHeight->itemText(i).toDouble())
                    {
                        min = fabs(ui->cbScrHeight->itemText(i).toDouble() - scrHeight);
                        min_idx = i;
                    }
                }

                // insert new item in appropriate place
                ui->cbScrHeight->insertItem(min_idx+2, QString("%1").arg(scrHeight));
                ui->cbScrHeight->setCurrentIndex( min_idx+2 );
            }
        }
    }

    // calculate screen area (m*m)
    ui->leArea->setText(QString("%L1").arg(scrWidth * scrHeight / 1.0e6));

    // calculate screen weight
    ui->leMetalWeightOne->setText(variables["metal_weight"].value.toString());

    if (calcMetalWeight)
        ui->leMetalWeight->setText( QString("%1").arg(ui->leMetalWeightOne->text().toDouble() * numModules) );

    ui->lePanelsWeight->setText( QString("%L1").arg(numModules * ui->leWeight->text().toDouble()) );

    QString rawPanelsWeight = ui->lePanelsWeight->text();
    rawPanelsWeight.remove(QLocale::system().groupSeparator());
    double panelsWeight = rawPanelsWeight.toDouble();

    ui->leScreenWeight->setText(QString("%L1").arg(ui->leMetalWeight->text().toDouble() + panelsWeight));

    // calculate screen power
    ui->leScrPowerAvg->setText(QString("%L1").arg(numModules * ui->lePowerAvg->text().toDouble()));
    ui->leScrPowerMax->setText(QString("%L1").arg(numModules * ui->lePowerMax->text().toDouble()));

    // calculate metal sum
    if (calcMetalSum)
        ui->leMetalSum->setText(QString("%1").arg(ui->leMetalPrice->text().toDouble() * numModules));

    // calculate resolution
    try {
        if ((ui->lePixelStep->text() != "") && (ui->lePixelStep->text().toDouble() != 0))
            resX = round(ui->leWidth->text().toDouble() / ui->lePixelStep->text().toDouble());
        else
            resX = 0;
    } catch (int) {
        resX = 0;
    }

    resX *= numModulesW;

    try {
        if ((ui->lePixelStep->text() != "") && (ui->lePixelStep->text().toDouble() != 0))
            resY = round(ui->leHeight->text().toDouble() / ui->lePixelStep->text().toDouble());
        else
            resY = 0;
    } catch (int) {
        resY = 0;
    }
    resY *= numModulesH;

    ui->leResolution->setText(QString("%1 x %2").arg(resX)
                                                .arg(resY)
                              );

    // calculate price
    totalPrice = 0;
    screenPrice = 0;

    QString rawArea = ui->leArea->text();
    rawArea.remove(QLocale::system().groupSeparator());
    double area = rawArea.toDouble();

    if (ui->chkUseOwnPrice->isChecked())
        screenPrice = ui->leOwnPrice->text().toDouble() * area;
    else
    {
        if (ui->cbModules->currentIndex() != -1)
        {
            if (ui->cbModPrice->currentIndex() == 0)
                screenPrice = (double) (modules.at(ui->cbModules->currentIndex()).off_price) * area;
            else if (ui->cbModPrice->currentIndex() == 1)
                screenPrice = (double) (modules.at(ui->cbModules->currentIndex()).min_price) * area;
        }
    }

    ui->leScreenSum->setText(QString("%L1").arg(screenPrice));

    totalPrice += screenPrice;

    totalPrice += ui->leMetalSum->text().toDouble();
    totalPrice += ui->leVideoPrice->text().toDouble();

    totalPrice += getDeliveryPrice();

    ui->leTotalPrice->setText(QString("%L1").arg(totalPrice));

    // create module name
    if (ui->cbModules->currentIndex() != -1)
        modName = modules.at(ui->cbModules->currentIndex()).name;
    else
        modName = "";

    // create screen name
    scrTitle = modName + "-";
    if (ui->cbPixelType->currentIndex() == PIXEL_INTERP)
        scrTitle += "i*.";
    //else
      //  scrTitle += "";

    scrTitle += QString("%1").arg(numModulesW * numModulesH * 4);

    scrTitleFile = scrTitle;
    scrTitleFile.replace("*", "");
}
// ----------------------------------------------------------------------------
/*
 *      TODO: make connects into signals:
 */
void MainWindow::on_leNumModulesX_editingFinished()
{
    calculateParams(true, true, false);
}

void MainWindow::on_leNumModulesY_editingFinished()
{
    calculateParams(true, true, false);
}

void MainWindow::on_cbScrWidth_currentIndexChanged(int index)
{
    UNUSED(index);
    calculateParams();
}

void MainWindow::on_cbScrHeight_currentIndexChanged(int index)
{
    UNUSED(index);
    calculateParams();
}

void MainWindow::on_leMetalPrice_textChanged(const QString &arg1)
{
    UNUSED(arg1);
    calculateParams();
}

void MainWindow::on_leVideoPrice_textChanged(const QString &arg1)
{
    UNUSED(arg1);
    calculateParams();
}

void MainWindow::on_cbModPrice_currentIndexChanged(int index)
{
    UNUSED(index);
    calculateParams();
}

void MainWindow::on_leOwnPrice_textChanged(const QString &arg1)
{
    UNUSED(arg1);
    calculateParams();
}

void MainWindow::on_leMetalSum_textChanged(const QString &arg1)
{
    UNUSED(arg1);
    calculateParams(false);
}

void MainWindow::on_leMetalWeight_textChanged(const QString &arg1)
{
    UNUSED(arg1);
    calculateParams(true, false);
}
// ----------------------------------------------------------------------------
// fill screen width, height from module sizes
void MainWindow::populateScreenDimensions()
{
    ui->cbScrWidth->clear();
    ui->cbScrHeight->clear();

    if (ui->leWidth->text().toDouble() != 0)
    {
        ui->cbScrWidth->addItem("");
        for (int i = 0; i < 250; i++)
            ui->cbScrWidth->addItem(QString("%1").arg(ui->leWidth->text().toDouble() * (i+1)));
    }

    if (ui->leHeight->text().toDouble() != 0)
    {
        ui->cbScrHeight->addItem("");
        for (int i = 0; i < 250; i++)
            ui->cbScrHeight->addItem(QString("%1").arg(ui->leHeight->text().toDouble() * (i+1)));
    }
}
// ----------------------------------------------------------------------------
void MainWindow::wordSearchReplace(QString oldString, QString newString)
{
    QAxObject *wordSelection = wordApp->querySubObject("Selection");

    QAxObject *find = wordSelection->querySubObject("Find");
    if (!find)
        return;

    find->dynamicCall("ClearFormatting()");

    QList<QVariant> params;
    params.operator << (QVariant(oldString));
    params.operator << (QVariant("0"));
    params.operator << (QVariant("0"));
    params.operator << (QVariant("0"));
    params.operator << (QVariant("0"));
    params.operator << (QVariant("0"));
    params.operator << (QVariant(true));
    params.operator << (QVariant("0"));
    params.operator << (QVariant("0"));
    params.operator << (QVariant(newString));
    params.operator << (QVariant("2"));
    params.operator << (QVariant("0"));
    params.operator << (QVariant("0"));
    params.operator << (QVariant("0"));
    params.operator << (QVariant("0"));

    find->dynamicCall("Execute(const QVariant&,const QVariant&,"
                      "const QVariant&,const QVariant&,"
                      "const QVariant&,const QVariant&,"
                      "const QVariant&,const QVariant&,"
                      "const QVariant&,const QVariant&,"
                      "const QVariant&,const QVariant&,"
                      "const QVariant&,const QVariant&,const QVariant&)",
                      params);

    delete find;
    delete wordSelection;
}
// ----------------------------------------------------------------------------
#include <QDebug>
void MainWindow::renderToWord(QString filename, int fmt)
{
    wordApp = new QAxObject("Word.Application");
    if (wordApp == NULL)
    {
        QMessageBox::warning(this, tr("Error"), tr("Unable to call Microsoft Word to process"));
        return;
    }

    wordApp->setProperty("DisplayAlerts", "0");

    // open template file
    QAxObject *documents = wordApp->querySubObject("Documents");

    QString templatePath = QCoreApplication::applicationDirPath() + "/templates/" + ui->cbTemplate->itemText(ui->cbTemplate->currentIndex());

    QAxObject *subObject = documents->querySubObject("Open(QString)", templatePath);

    // abort rendering if template not exists (fail to open)
    if (!subObject) return;

    // Save template file as new
    // format: 17 = pdf, 16 = docx, 0 = doc
    QAxObject * document = wordApp->querySubObject("ActiveDocument");
    QFile::remove(QCoreApplication::applicationDirPath() + "/templates/$$temp$$.docx");

    document->dynamicCall("SaveAs (const QString&, int)", QCoreApplication::applicationDirPath() + "/templates/$$temp$$.docx", 16);

    // perform replace
    wordSearchReplace("{screen_width}", QString("%L1").arg(scrWidth));
    wordSearchReplace("{screen_height}", QString("%L1").arg(scrHeight));
    wordSearchReplace("{screen_area}", ui->leArea->text());
    wordSearchReplace("{screen_weight}", ui->leScreenWeight->text());
    wordSearchReplace("{screen_power_max}", ui->leScrPowerMax->text());
    wordSearchReplace("{screen_power_avg}", ui->leScrPowerAvg->text());
    wordSearchReplace("{resolution_x}", QString("%L1").arg(resX));
    wordSearchReplace("{resolution_y}", QString("%L1").arg(resY));
    wordSearchReplace("{num_modules_w}", QString("%1").arg(numModulesW));
    wordSearchReplace("{num_modules_h}", QString("%1").arg(numModulesH));

    if (ui->cbLedType->currentIndex() == LED_DIP)
    {
        wordSearchReplace("{angles_text}", variables["angles_text_dip"].value.toString());
        wordSearchReplace("{angles_value}", variables["angles_value_dip"].value.toString());
        wordSearchReplace("{geometry_value}", variables["geometry_value_dip"].value.toString());
    }
    else if (ui->cbLedType->currentIndex() == LED_SMD)
    {
        wordSearchReplace("{angles_text}", variables["angles_text_smd"].value.toString());
        wordSearchReplace("{angles_value}", variables["angles_value_smd"].value.toString());
        wordSearchReplace("{geometry_value}", variables["geometry_value_smd"].value.toString());
    }
    else
    {
        wordSearchReplace("{angles_text}", "");
        wordSearchReplace("{angles_value}", "");
        wordSearchReplace("{geometry_value}", "");
    }

    wordSearchReplace("{read_distance}", QString("%1").arg((int)ui->lePixelStep->text().toDouble()));

    if (ui->cbPixelType->currentIndex() == PIXEL_INTERP)
    {
        wordSearchReplace("{interp_desc}", variables["interp_desc"].value.toString());
        wordSearchReplace("{pxstep_desc}", variables["pxstep_desc"].value.toString());
    }
    else
    {
        wordSearchReplace("{interp_desc}", "");
        wordSearchReplace("{pxstep_desc}", "");
    }

    wordSearchReplace("{video_price}", ui->leVideoPrice->text());

    if (ui->cbDeliveryPrice->currentIndex() == 0)
        wordSearchReplace("{install_price}", variables["install_text"].value.toString());
    else
        wordSearchReplace("{install_price}", QString("%L1").arg(getDeliveryPrice()));

    wordSearchReplace("{metal_price}", QString("%L1").arg(ui->leMetalPrice->text().toDouble()));
    wordSearchReplace("{metal_sum}", QString("%L1").arg(ui->leMetalSum->text().toDouble()));
    wordSearchReplace("{screen_price}", QString("%L1").arg(screenPrice));
    wordSearchReplace("{total_price}", QString("%L1").arg(totalPrice));

    wordSearchReplace("{module_name}", modName);

    wordSearchReplace("{module_w}", ui->leWidth->text());
    wordSearchReplace("{module_h}", ui->leHeight->text());
    wordSearchReplace("{matrix_w}", ui->leMatrixWidth->text());
    wordSearchReplace("{matrix_h}", ui->leMatrixHeight->text());
    wordSearchReplace("{led_model}", ui->cbLedModel->currentText());
    wordSearchReplace("{led_type}", ui->cbLedType->currentText());
    wordSearchReplace("{pixel_type}", ui->cbPixelType->currentText());
    wordSearchReplace("{pixel_step}", ui->lePixelStep->text());
    wordSearchReplace("{module_weight}", ui->leWeight->text());
    wordSearchReplace("{module_power_max}", ui->lePowerMax->text());
    wordSearchReplace("{module_power_avg}", ui->lePowerAvg->text());
    wordSearchReplace("{brightness}", QString("%L1").arg(ui->leBrightness->text().toDouble()));

    wordSearchReplace("{screen_title}", scrTitle);

    wordSearchReplace("{value}", variables["value"].value.toString());

    wordSearchReplace("{app_version}", QString("%1").arg(__DATE__));

    // embed image into document
    RenderArea *renderArea = new RenderArea;

    renderArea->setParams(
                numModulesW,
                numModulesH,
                scrWidth,
                scrHeight,
                ui->leWidth->text().toDouble(),
                ui->leHeight->text().toDouble(),
                scrTitle,
                resX,
                resY,
                numModulesW * numModulesH * ui->lePowerAvg->text().toDouble(),
                (variables["show_overlay"].value.toInt() == 0)?false:true
    );

    // get image data
    renderArea->copyToClipboard();

    delete renderArea;

    // move to end of document
    QAxObject *wordSelection = wordApp->querySubObject("Selection");

    wordSelection->dynamicCall("GoTo (QVariant, QVariant, QVariant, QVariant)",
                            QVariant(3),    // WdGoToItem.wdGoToLine
                            QVariant(-1),   // WdGoToDirection.wdGoToLast
                            QVariant(1),
                            QVariant(0)
                          );

    // insert page break
    wordSelection->dynamicCall("InsertBreak (int)",
                               7    // wdPageBreak
    );

    // paste image data
    wordSelection->dynamicCall("Paste ()");

    delete wordSelection;

    // print button pressed
    if (fmt == WORD_PRINTING)
    {
        QAxObject *dialogs = wordApp->querySubObject("Dialogs");
        QAxObject *item = dialogs->querySubObject("Item(int)", WORD_PRINT_DIALOG);

        document->dynamicCall("Save ()");   // save temporary file to prevent alerts from word while closing

        wordApp->setProperty("Visible", true);
        item->dynamicCall("Show()");
        wordApp->setProperty("Visible", false);

        delete item;
        delete dialogs;
    }
    else
    {
        document->dynamicCall("SaveAs (const QString&, int)", filename, fmt);
    }

    document->dynamicCall("Close()");
    delete document;

    wordApp->dynamicCall("Quit()");

    delete documents;
    delete wordApp;

    QFile::remove(QCoreApplication::applicationDirPath() + "/templates/$$temp$$.docx");
}
// ----------------------------------------------------------------------------
void MainWindow::on_btnImage_clicked()
{
    if ((numModulesH == 0) ||
            (numModulesW == 0) ||
            (scrWidth == 0) ||
            (scrHeight == 0)
    )
    {
        QMessageBox::warning(this, tr("Error"), tr("Wrong screen parameters"));
        return;
    }

    QString fileName = QFileDialog::getSaveFileName(this,
                                                tr("Save image as"),
                                                    QCoreApplication::applicationDirPath() + "/" + exportDir + "/" + scrTitleFile + ".png",
                                                "PNG image (*.png);;JPEG image (*.jpg);;Windows Bitmap (*.bmp)");
    if (fileName != "")
    {
        RenderArea *renderArea = new RenderArea;

        renderArea->setParams(
                    numModulesW,
                    numModulesH,
                    scrWidth,
                    scrHeight,
                    ui->leWidth->text().toDouble(),
                    ui->leHeight->text().toDouble(),
                    scrTitle,
                    resX,
                    resY,
                    numModulesW * numModulesH * ui->lePowerAvg->text().toDouble(),
                    (variables["show_overlay"].value.toInt() == 0)?false:true
        );

        renderArea->saveToFile(fileName);

        delete renderArea;
    }

}
// ----------------------------------------------------------------------------
// EOF
