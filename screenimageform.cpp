#include <QGridLayout>
#include "screenimageform.h"
#include "ui_screenimageform.h"
#include "renderarea.h"

ScreenImageForm::ScreenImageForm(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ScreenImageForm)
{
    ui->setupUi(this);

    renderArea = new RenderArea;
/*
    renderArea->setParams(
                10,
                10,
                100,
                100,
                400,
                300,
                "Screen title",
                1024,
                768,
                1000
    );
*/
    QGridLayout *layout = new QGridLayout;
    layout->addWidget(renderArea);
    ui->scrollArea->setLayout(layout);
}

ScreenImageForm::~ScreenImageForm()
{
    delete renderArea;
    delete ui;
}

void ScreenImageForm::setParams(
        double numModulesW,
        double numModulesH,
        double screenW,
        double screenH,
        double modW,
        double modH,
        QString title,
        int resX,
        int resY,
        double powerAvg
)
{
    renderArea->setParams(
                numModulesW,
                numModulesH,
                screenW,
                screenH,
                modW,
                modH,
                title,
                resX,
                resY,
                powerAvg
    );

    renderArea->update();
    renderArea->adjustSize();
}

void ScreenImageForm::on_toolButton_clicked()
{
    renderArea->saveToFile(ui->lineEdit->text());
}
