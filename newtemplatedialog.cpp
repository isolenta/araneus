#include "newtemplatedialog.h"
#include "ui_newtemplatedialog.h"

NewTemplateDialog::NewTemplateDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::NewTemplateDialog)
{
    ui->setupUi(this);
}

NewTemplateDialog::~NewTemplateDialog()
{
    delete ui;
}
