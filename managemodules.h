#ifndef MANAGEMODULES_H
#define MANAGEMODULES_H

#include <QDialog>
#include "moduleeditor.h"
#include <QMap>

class Module;
class Variable;

namespace Ui {
class ManageModules;
}

class ManageModules : public QDialog
{
    Q_OBJECT
    
public:
    explicit ManageModules(QWidget *parent);
    ~ManageModules();
    
    void showEvent (QShowEvent* event);
    void hideEvent (QHideEvent* event);
    QStringList *ledModels;
    QString valueSign;

    void updatePriceLabels(QString);
    QMap<QString, Variable> *variables;

private slots:
    void on_toolButton_clicked();


private:
    Ui::ManageModules *ui;
    QList<Module> *modules;
    ModuleEditor *meditor;

public slots:
    void slotModuleEdit(Module *);
    void slotModuleRemove(Module *);
};

#endif // MANAGEMODULES_H
