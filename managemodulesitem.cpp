#include "managemodulesitem.h"
#include <QMouseEvent>

ManageModulesItem::ManageModulesItem(QWidget *parent, Module *_modptr) :
    QFrame(parent)
{
    gridLayout = new QGridLayout(this);
    gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
    horizontalLayout = new QHBoxLayout();
    horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
    lName = new QLabel(this);
    lName->setObjectName(QString::fromUtf8("lName"));
    QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
    sizePolicy.setHorizontalStretch(0);
    sizePolicy.setVerticalStretch(0);
    sizePolicy.setHeightForWidth(lName->sizePolicy().hasHeightForWidth());
    lName->setSizePolicy(sizePolicy);

    horizontalLayout->addWidget(lName);

    btnEdit = new QToolButton(this);
    btnEdit->setObjectName(QString::fromUtf8("btnEdit"));

    QIcon icon1;
    icon1.addFile(QString::fromUtf8(":/icons/icons/pencil.png"), QSize(), QIcon::Normal, QIcon::Off);
    btnEdit->setIcon(icon1);
    btnEdit->setIconSize(QSize(16, 16));
    btnEdit->setAutoRaise(true);
    btnEdit->setToolTip(tr("Edit"));

    horizontalLayout->addWidget(btnEdit);

    btnRemove = new QToolButton(this);
    btnRemove->setObjectName(QString::fromUtf8("btnRemove"));

    QIcon icon2;
    icon2.addFile(QString::fromUtf8(":/icons/icons/remove.png"), QSize(), QIcon::Normal, QIcon::Off);
    btnRemove->setIcon(icon2);
    btnRemove->setIconSize(QSize(16, 16));
    btnRemove->setAutoRaise(true);
    btnRemove->setToolTip(tr("Remove"));

    horizontalLayout->addWidget(btnRemove);

    gridLayout->addLayout(horizontalLayout, 0, 2, 1, 1);

    connect(btnEdit, SIGNAL(clicked()), this, SLOT(btnEdit_clicked()));
    connect(btnRemove, SIGNAL(clicked()), this, SLOT(btnRemove_clicked()));

    modptr = _modptr;
}

ManageModulesItem::~ManageModulesItem()
{

}

void ManageModulesItem::mouseDoubleClickEvent(QMouseEvent *e)
{
    Q_UNUSED(e);

    emit btnEditSignal(modptr);
}

void ManageModulesItem::setLabelText(QString text)
{
    lName->setText(text);
}

void ManageModulesItem::btnEdit_clicked()
{
    emit btnEditSignal(modptr);
}

void ManageModulesItem::btnRemove_clicked()
{
    emit btnRemoveSignal(modptr);
}
