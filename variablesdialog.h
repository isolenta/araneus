#ifndef VARIABLESDIALOG_H
#define VARIABLESDIALOG_H

#include <QDialog>
#include <QMap>
#include <QVariant>

#include "mainwindow.h"

namespace Ui {
    class VariablesDialog;
}

class Variable;

class VariablesDialog : public QDialog
{
    Q_OBJECT

public:
    explicit VariablesDialog(QWidget *parent = 0);
    ~VariablesDialog();

    private slots:
        void on_buttonBox_accepted();

private:
    Ui::VariablesDialog *ui;
    QMap<QString, Variable> *variables;

    void showEvent (QShowEvent* event);
    void hideEvent (QHideEvent* event);
};

#endif // VARIABLESDIALOG_H
