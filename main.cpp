#include <QtGui/QApplication>
#include <QTranslator>
#include "mainwindow.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QTranslator translator;
    translator.load("russian", "config");
    a.installTranslator(&translator);
    a.setStyleSheet("QStatusBar::item { border: none;}");

    MainWindow w;
    w.show();
    
    return a.exec();
}
