#include "moduleeditor.h"
#include "ui_moduleeditor.h"

#include "mainwindow.h"

#include <QDoubleValidator>
#include <QMessageBox>

ModuleEditor::ModuleEditor(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ModuleEditor)
{
    ui->setupUi(this);

    doubleInput = new QDoubleValidator(this);

    ui->leWidth->setValidator(doubleInput);
    ui->leHeight->setValidator(doubleInput);
    ui->leOffPrice->setValidator(doubleInput);
    ui->leMinPrice->setValidator(doubleInput);
    ui->lePixelStep->setValidator(doubleInput);
    ui->leMatrixHeight->setValidator(doubleInput);
    ui->leMatrixWidth->setValidator(doubleInput);
    ui->lePowerAvg->setValidator(doubleInput);
    ui->lePowerMax->setValidator(doubleInput);
    ui->leBrightness->setValidator(doubleInput);
    ui->leWeight->setValidator(doubleInput);

    ui->leWidth->setStyleSheet("QLineEdit {border: 1px solid black;}");
    ui->leHeight->setStyleSheet("QLineEdit {border: 1px solid black;}");
    ui->leOffPrice->setStyleSheet("QLineEdit {border: 1px solid black;}");
    ui->leMinPrice->setStyleSheet("QLineEdit {border: 1px solid black;}");
    ui->lePixelStep->setStyleSheet("QLineEdit {border: 1px solid black;}");
    ui->leMatrixHeight->setStyleSheet("QLineEdit {border: 1px solid black;}");
    ui->leMatrixWidth->setStyleSheet("QLineEdit {border: 1px solid black;}");
    ui->lePowerAvg->setStyleSheet("QLineEdit {border: 1px solid black;}");
    ui->lePowerMax->setStyleSheet("QLineEdit {border: 1px solid black;}");
    ui->leBrightness->setStyleSheet("QLineEdit {border: 1px solid black;}");
    ui->leWeight->setStyleSheet("QLineEdit {border: 1px solid black;}");
    ui->leName->setStyleSheet("QLineEdit {border: 1px solid black;}");
}

ModuleEditor::~ModuleEditor()
{
    delete doubleInput;
    delete ui;
}

void ModuleEditor::setModulePtr(Module *_modptr)
{
    modptr = _modptr;
}

void ModuleEditor::on_btnOk_clicked()
{
    modptr->name = ui->leName->text();
    modptr->width = ui->leWidth->text().toDouble();
    modptr->height = ui->leHeight->text().toDouble();
    modptr->off_price = ui->leOffPrice->text().toDouble();
    modptr->min_price = ui->leMinPrice->text().toDouble();
    modptr->pixel_step = ui->lePixelStep->text().toDouble();

    if (ui->cbLedType->currentIndex() == 0)
        modptr->led_type = LED_DIP;
    else
        modptr->led_type = LED_SMD;

    if (ui->cbPixelType->currentIndex() == 0)
        modptr->pixel_type = PIXEL_DEFAULT;
    else
        modptr->pixel_type = PIXEL_INTERP;

    modptr->matrix_height = ui->leMatrixHeight->text().toDouble();
    modptr->matrix_width = ui->leMatrixWidth->text().toDouble();
    modptr->power_avg = ui->lePowerAvg->text().toDouble();
    modptr->power_max = ui->lePowerMax->text().toDouble();
    modptr->brightness = ui->leBrightness->text().toDouble();
    modptr->weight = ui->leWeight->text().toDouble();
    modptr->led_model = ui->cbLedModel->itemText( ui->cbLedModel->currentIndex() );

    QStringList *ledModels = qobject_cast<ManageModules *>(parentWidget())->ledModels;

    ledModels->clear();
    for (int i = 0; i < ui->cbLedModel->count(); i++)
        ledModels->append( ui->cbLedModel->itemText(i));

    qobject_cast<ManageModules *>(parentWidget())->hideEvent(0);
    qobject_cast<ManageModules *>(parentWidget())->showEvent(0);

    hide();
}

void ModuleEditor::hideEvent (QHideEvent* event)
{
    UNUSED(event);

    ui->cbLedType->clear();
    ui->cbPixelType->clear();
}

void ModuleEditor::showEvent (QShowEvent* event)
{
    UNUSED(event);

    // update value sign for labels
    QString valueSign = qobject_cast<ManageModules *>(parentWidget())->valueSign;
    ui->label_4->setText( tr("Official price") + QString(" (%1)").arg(valueSign));
    ui->label_5->setText( tr("Minimal price") + QString(" (%1)").arg(valueSign));

    ui->leName->setText(modptr->name);
    ui->leWidth->setText(QString("%1").arg(modptr->width));
    ui->leHeight->setText(QString("%1").arg(modptr->height));
    ui->leOffPrice->setText(QString("%1").arg(modptr->off_price));
    ui->leMinPrice->setText(QString("%1").arg(modptr->min_price));
    ui->lePixelStep->setText(QString("%1").arg(modptr->pixel_step));

    ui->cbLedType->addItem(tr("DIP"));
    ui->cbLedType->addItem(tr("SMD"));
    if (modptr->led_type == LED_SMD)
    {
        ui->cbLedType->setCurrentIndex(1);
        ui->cbPixelType->setEnabled(false);
    }
    else
    {
        ui->cbLedType->setCurrentIndex(0);
        ui->cbPixelType->setEnabled(true);
    }

    ui->cbPixelType->addItem(tr("default"));
    ui->cbPixelType->addItem(tr("interpolate"));
    if (modptr->pixel_type == PIXEL_INTERP)
        ui->cbPixelType->setCurrentIndex(1);
    else
        ui->cbPixelType->setCurrentIndex(0);

    ui->leMatrixHeight->setText(QString("%1").arg(modptr->matrix_height));
    ui->leMatrixWidth->setText(QString("%1").arg(modptr->matrix_width));
    ui->lePowerAvg->setText(QString("%1").arg(modptr->power_avg));
    ui->lePowerMax->setText(QString("%1").arg(modptr->power_max));
    ui->leBrightness->setText(QString("%1").arg(modptr->brightness));
    ui->leWeight->setText(QString("%1").arg(modptr->weight));

    QStringList *ledModels = qobject_cast<ManageModules *>(parentWidget())->ledModels;

    ui->cbLedModel->clear();
    for (int i = 0; i < ledModels->count(); i++)
        ui->cbLedModel->addItem(ledModels->at(i));

    bool lm_found = false;
    for (int i = 0; i < ui->cbLedModel->count(); i++)
    {
        if (ui->cbLedModel->itemText(i) == modptr->led_model)
        {
            ui->cbLedModel->setCurrentIndex(i);
            lm_found = true;
            break;
        }
    }

    if (!lm_found && modptr->led_model != "")
    {
        ui->cbLedModel->addItem(modptr->led_model);
        ui->cbLedModel->setCurrentIndex( ui->cbLedModel->count() - 1 );
    }
}

void ModuleEditor::on_cbLedType_currentIndexChanged(int index)
{
    if (index == 1) // SMD selected
    {
        ui->cbPixelType->setCurrentIndex(0); // set to PIXEL_DEFAULT
        ui->cbPixelType->setEnabled(false);
    }
    else
    {
        ui->cbPixelType->setEnabled(true);
    }
}

void ModuleEditor::on_btnCancel_clicked()
{
    hide();
}
