#ifndef MANAGEMODULESITEM_H
#define MANAGEMODULESITEM_H

#include <QFrame>
#include <QGridLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QToolButton>

namespace Ui {
class ManageModulesItem;
}

class Module;

class ManageModulesItem : public QFrame
{
    Q_OBJECT
    
public:
    explicit ManageModulesItem(QWidget *parent, Module *_modptr);
    ~ManageModulesItem();
    void setLabelText(QString text);
    
    void mouseDoubleClickEvent(QMouseEvent *e);

private:
    QGridLayout *gridLayout;
    QHBoxLayout *horizontalLayout;
    QLabel *lName;
    QToolButton *btnEdit;
    QToolButton *btnRemove;

    Module *modptr;

signals:
    void btnEditSignal(Module *module);
    void btnRemoveSignal(Module *module);

public slots:
    void btnEdit_clicked();
    void btnRemove_clicked();
};

#endif // MANAGEMODULESITEM_H
