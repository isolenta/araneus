#ifndef MODULEEDITOR_H
#define MODULEEDITOR_H

#include <QMainWindow>
//#include "mainwindow.h"
#include <QDoubleValidator>


namespace Ui {
class ModuleEditor;
}

class Module;

class ModuleEditor : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit ModuleEditor(QWidget *parent);
    ~ModuleEditor();

    void hideEvent (QHideEvent* event);
    void showEvent (QShowEvent* event);
    void setModulePtr(Module *_modptr);

    
private slots:
    void on_btnOk_clicked();

    void on_cbLedType_currentIndexChanged(int index);

    void on_btnCancel_clicked();


private:
    Ui::ModuleEditor *ui;
    Module *modptr;

    QDoubleValidator *doubleInput;
};

#endif // MODULEEDITOR_H
