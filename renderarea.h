#ifndef RENDERAREA_H
#define RENDERAREA_H

#include <QWidget>
#include <QImage>

class RenderArea : public QWidget
{
    Q_OBJECT
public:
    explicit RenderArea(QWidget *parent = 0);
    void setParams(
            double numModulesW,
            double numModulesH,
            double screenW,
            double screenH,
            double modW,
            double modH,
            QString title,
            int resX,
            int resY,
            double powerAvg,
            bool showOverlay
    );

    void saveToFile(QString filename);
    void copyToClipboard();

signals:
protected:
    void paintEvent(QPaintEvent *event);

public slots:

private:
    static const int MODNUM_LIMIT = 20;
    static const int MAXLINESIZE = 600;
    static const int PADDING = 100;
    static const int SHADOW_OFFSET = 6;
    static const int SCREEN_LINE_WEIGHT = 2;
    static const int SIZE_OUTER = 40;
    static const int ARROW_H = 10;
    static const int ARROW_W = 4;
    static const int OVERLAY_W = 350;
    static const int OVERLAY_H = 200;

    double numModulesW;
    double numModulesH;
    double screenW;
    double screenH;
    double modW;
    double modH;
    QString title;
    int resX;
    int resY;
    double powerAvg;
    bool showOverlay;

    int drawableModNumW, drawableModNumH;
    int drawableScreenW, drawableScreenH;
    int drawableModWidth, drawableModHeight;
    int imageWidth, imageHeight;
    int overlayPosX, overlayPosY;
    int screenPosX, screenPosY;
    bool overlayBorder;

    QImage *image;

    void drawArrow(QPainter *p, int x1, int y1, int x2, int y2);
    void drawTextRotated(QPainter *p, const QPoint &point, const QString &s, int angle);
    void paintImage(QPainter *painter, bool forDocument = false);
};

#endif // RENDERAREA_H
