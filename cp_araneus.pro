#-------------------------------------------------
#
# Project created by QtCreator 2013-06-30T16:19:05
#
#-------------------------------------------------

QT       += core gui

TARGET = cp_araneus
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    managemodules.cpp \
    managemodulesitem.cpp \
    variablesdialog.cpp \
    moduleeditor.cpp \
    renderarea.cpp

HEADERS  += mainwindow.h \
    managemodules.h \
    managemodulesitem.h \
    variablesdialog.h \
    moduleeditor.h \
    renderarea.h

FORMS    += mainwindow.ui \
    managemodules.ui \
    variablesdialog.ui \
    moduleeditor.ui

CONFIG += qaxcontainer
LIBS += -static-libgcc -lntdll -static

RESOURCES += \
    resources.qrc

TRANSLATIONS += russian.ts
RC_FILE = araneus.rc











