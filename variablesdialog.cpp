#include <QMessageBox>
#include "variablesdialog.h"
#include "ui_variablesdialog.h"
#include "mainwindow.h"

VariablesDialog::VariablesDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::VariablesDialog)
{
    ui->setupUi(this);

    variables = &(qobject_cast<MainWindow *>(parentWidget())->variables);

    ui->tableWidget->setHorizontalHeaderItem(0, new QTableWidgetItem(tr("Name")));
    ui->tableWidget->setHorizontalHeaderItem(1, new QTableWidgetItem(tr("Value")));
    ui->tableWidget->setHorizontalHeaderItem(2, new QTableWidgetItem(tr("Description")));

    ui->tableWidget->verticalHeader()->hide();
    ui->tableWidget->horizontalHeader()->setResizeMode(QHeaderView::Stretch);
}

VariablesDialog::~VariablesDialog()
{
    delete ui;
}

void VariablesDialog::on_buttonBox_accepted()
{
    for (int i = 0; i < ui->tableWidget->rowCount(); i++)
    {
        Variable var;

        var.value = ui->tableWidget->item(i, 1)->text();
        var.desc  = ui->tableWidget->item(i, 2)->text();

       (*variables)[ui->tableWidget->item(i, 0)->text()] = var;
    }

    qobject_cast<MainWindow *>(parentWidget())->onVariablesUpdated();
}

void VariablesDialog::showEvent (QShowEvent* event)
{
    UNUSED(event);

    QMap<QString, Variable>::const_iterator it = variables->begin();

    ui->tableWidget->setRowCount(0);

    while (it != variables->end())
    {
        ui->tableWidget->setRowCount( ui->tableWidget->rowCount() + 1);

        // create name item
        QTableWidgetItem *item_name = new QTableWidgetItem;
        item_name->setText(it.key());
        item_name->setFlags(item_name->flags() ^ (Qt::ItemIsEditable | Qt::ItemIsSelectable));
        ui->tableWidget->setItem(ui->tableWidget->rowCount() - 1, 0, item_name);

        // create value item
        QTableWidgetItem *item_value = new QTableWidgetItem;
        item_value->setText(it.value().value.toString());
        ui->tableWidget->setItem(ui->tableWidget->rowCount() - 1, 1, item_value);

        // create description item
        QTableWidgetItem *item_desc = new QTableWidgetItem;
        item_desc->setText(it.value().desc);
        ui->tableWidget->setItem(ui->tableWidget->rowCount() - 1, 2, item_desc);

        ++it;
    }
}

void VariablesDialog::hideEvent (QHideEvent* event)
{
    UNUSED(event);
}
