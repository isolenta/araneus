#include <QPainter>
#include <QImage>
#include <QMimeData>
#include <QBuffer>
#include <QClipboard>
#include <QApplication>

#include <math.h>

#include "renderarea.h"
#include "mainwindow.h"

RenderArea::RenderArea(QWidget *parent) :
    QWidget(parent)
{
}

void RenderArea::setParams(
        double numModulesW,
        double numModulesH,
        double screenW,
        double screenH,
        double modW,
        double modH,
        QString title,
        int resX,
        int resY,
        double powerAvg,
        bool showOverlay
)
{
    // setup screen params
    this->numModulesW = numModulesW;
    this->numModulesH = numModulesH;
    this->screenW = screenW;
    this->screenH = screenH;
    this->modW = modW;
    this->modH = modH;
    this->title = title;
    this->resX = resX;
    this->resY = resY;
    this->powerAvg = powerAvg;
    this->showOverlay = showOverlay;

    // calculate image dimensions
    if (numModulesW > MODNUM_LIMIT)
        drawableModNumW = MODNUM_LIMIT;
    else
        drawableModNumW = numModulesW;

    if (numModulesH > MODNUM_LIMIT)
        drawableModNumH = MODNUM_LIMIT;
    else
        drawableModNumH = numModulesH;

    if (screenW > screenH)
    {
        drawableScreenW = MAXLINESIZE;
        drawableScreenH = (int)( (double)MAXLINESIZE * screenH / screenW );
    }
    else
    {
        drawableScreenH = MAXLINESIZE;
        drawableScreenW = (int)( (double)MAXLINESIZE * screenW / screenH );
    }

    if (drawableModNumW != 0)
    {
        drawableModWidth = drawableScreenW / drawableModNumW;
        drawableModWidth = drawableScreenW / drawableModNumW;
    }
    else
    {
        drawableModWidth = 0;
        drawableModWidth = 0;
    }

    if (drawableModNumH != 0)
    {
        drawableModHeight = drawableScreenH / drawableModNumH;
        drawableModHeight = drawableScreenH / drawableModNumH;
    }
    else
    {
        drawableModHeight = 0;
        drawableModHeight = 0;
    }

    imageWidth = PADDING * 2 + drawableScreenW;
    imageHeight = PADDING * 2 + drawableScreenH;

    screenPosX = PADDING;
    screenPosY = PADDING;

    if (showOverlay)
    {
        overlayBorder = false;

        if (drawableScreenW < OVERLAY_W)
        {
            imageWidth += OVERLAY_W;
            overlayPosX = PADDING * 1.5 + drawableScreenW;
            overlayBorder = true;
        }
        else
            overlayPosX = PADDING + SCREEN_LINE_WEIGHT + (drawableScreenW - OVERLAY_W) / 2;

        if (drawableScreenH < OVERLAY_H)
        {
            imageHeight += OVERLAY_H;
            overlayPosY = PADDING * 0.5;
            screenPosY = PADDING + OVERLAY_H;
            overlayBorder = true;

        }
        else
            overlayPosY = PADDING + SCREEN_LINE_WEIGHT + (drawableScreenH - OVERLAY_H) / 2;
    } // showOverlay

    QRect rect = this->geometry();
    rect.setWidth(imageWidth);
    rect.setHeight(imageHeight);
    this->setGeometry(rect);
}

void RenderArea::drawArrow(QPainter *p, int x1, int y1, int x2, int y2)
{
    double Vx, Vy, V;

    Vx = x2 - x1;
    Vy = y2 - y1;
    V = sqrt(Vx*Vx + Vy*Vy);
    Vx = Vx / V;
    Vy = Vy / V;

    double nx = Vy;
    double ny = -Vx;

    int x3 = x2 - ARROW_H * Vx;
    int y3 = y2 - ARROW_H * Vy;

    int x4 = x3 + ARROW_W * nx;
    int y4 = y3 + ARROW_W * ny;

    int x5 = x3 - ARROW_W * nx;
    int y5 = y3 - ARROW_W * ny;

    const QPointF points[5] = {
        QPointF(x1, y1),
        QPointF(x2, y2),
        QPointF(x4, y4),
        QPointF(x5, y5),
        QPointF(x2, y2)
    };

    QBrush brush;
    brush.setColor(Qt::black);
    p->setBrush(Qt::SolidPattern);
    p->drawPolygon(points, 5);
}

void RenderArea::drawTextRotated(QPainter *p, const QPoint &point, const QString &s, int angle)
{
    QFontMetrics fm(p->font());

    int w = fm.width(s) + 2;
    int h = fm.height();

    QPixmap pm(w, w);
    pm.fill(QColor(0,0,0,0));

    QPainter ppm (&pm);
    ppm.initFrom (this);
    ppm.translate(w/2, w/2);
    ppm.rotate (angle);
    ppm.drawText (-w/2, h/4, s);
    ppm.end ();

    p->drawPixmap(point, pm);
}

void RenderArea::paintImage(QPainter *painter, bool forDocument)
{
    QPen pen;
    QColor color(0, 0, 0);

    pen.setColor(Qt::black);
    pen.setWidth(1);

    painter->setPen(pen);

    // area borders
    painter->fillRect(0, 0, imageWidth, imageHeight, Qt::white);

    if (!forDocument)
        painter->drawRect(0, 0, imageWidth, imageHeight);

    // shadow
    QBrush brush;
    brush.setColor(QColor(0,0,0));
    painter->fillRect(screenPosX+SHADOW_OFFSET, screenPosY-SHADOW_OFFSET, drawableScreenW, drawableScreenH, Qt::gray);

    // screen rect
    pen.setWidth(SCREEN_LINE_WEIGHT); painter->setPen(pen);

    QBrush areaBrush;
    areaBrush.setColor(QColor(0xdd, 0xdd, 0xdd));
    areaBrush.setStyle(Qt::Dense1Pattern);
    painter->fillRect(screenPosX, screenPosY, drawableScreenW, drawableScreenH, areaBrush);
    painter->drawRect(screenPosX, screenPosY, drawableScreenW, drawableScreenH);

    // modules
    pen.setWidth(1); painter->setPen(pen);
    for (int i = 1; i < drawableModNumW; i++)
    {
        painter->drawLine(screenPosX + SCREEN_LINE_WEIGHT + i * drawableModWidth,
                          screenPosY + SCREEN_LINE_WEIGHT / 2,
                          screenPosX + SCREEN_LINE_WEIGHT + i * drawableModWidth,
                          screenPosY + drawableScreenH - SCREEN_LINE_WEIGHT / 2
        );
    }

    for (int i = 1; i < drawableModNumH; i++)
    {
        painter->drawLine(screenPosX + SCREEN_LINE_WEIGHT / 2,
                          screenPosY + i * drawableModHeight + SCREEN_LINE_WEIGHT / 2,
                          screenPosX + drawableScreenW - SCREEN_LINE_WEIGHT / 2,
                          screenPosY + i * drawableModHeight + SCREEN_LINE_WEIGHT / 2
        );
    }

    // sizes - screen vertical
    painter->drawLine(screenPosX,
                      screenPosY,
                      screenPosX - SIZE_OUTER,
                      screenPosY);

    painter->drawLine(screenPosX,
                      screenPosY + drawableScreenH,
                      screenPosX - SIZE_OUTER,
                      screenPosY + drawableScreenH);

    painter->drawLine(screenPosX - SIZE_OUTER * 0.75,
                      screenPosY + 1,
                      screenPosX - SIZE_OUTER * 0.75,
                      screenPosY + drawableScreenH - 1);

    drawArrow(painter,
              screenPosX - SIZE_OUTER * 0.75,
              screenPosY + ARROW_H,
              screenPosX - SIZE_OUTER * 0.75,
              screenPosY);

    drawArrow(painter,
              screenPosX - SIZE_OUTER * 0.75,
              screenPosY + drawableScreenH - ARROW_H,
              screenPosX - SIZE_OUTER * 0.75,
              screenPosY + drawableScreenH);

    // sizes - screen horizontal
    painter->drawLine(screenPosX - 1,
                      screenPosY - 1,
                      screenPosX - 1,
                      screenPosY - SIZE_OUTER);

    painter->drawLine(screenPosX + drawableScreenW,
                      screenPosY,
                      screenPosX + drawableScreenW,
                      screenPosY - SIZE_OUTER);

    painter->drawLine(screenPosX + 1,
                      screenPosY - SIZE_OUTER * 0.75,
                      screenPosX + drawableScreenW - 1,
                      screenPosY - SIZE_OUTER * 0.75);

    drawArrow(painter,
              screenPosX + ARROW_H,
              screenPosY - SIZE_OUTER * 0.75,
              screenPosX,
              screenPosY - SIZE_OUTER * 0.75);

    drawArrow(painter,
              screenPosX + drawableScreenW - ARROW_H,
              screenPosY - SIZE_OUTER * 0.75,
              screenPosX + drawableScreenW,
              screenPosY - SIZE_OUTER * 0.75);

    QFont font = painter->font();
    font.setFamily("Courier new");
    painter->setFont(font);

    // text - screen width
    painter->drawText(QRect(screenPosX,
                            screenPosY - SIZE_OUTER - 2, drawableScreenW, SIZE_OUTER * 0.25 - 2
                      ),
                      Qt::TextSingleLine | Qt::AlignHCenter | Qt::AlignVCenter,
                      QString("%1").arg(screenW));

    // text - screen height
    QString str = QString("%1").arg(screenH);
    QFontMetrics fm(painter->font()); int w = fm.width(str);
    drawTextRotated(painter, QPoint(screenPosX - SIZE_OUTER - w/2 + 2, screenPosY + drawableScreenH - w - drawableScreenH/2 + w/2), str, 270);

    int corrW = ((drawableModNumW > 1) ? 1 : 0);
    int corrH = ((drawableModNumH > 1) ? 1 : 0);

    if (drawableModNumW > 1)
    {
        // sizes - module horizontal
        painter->drawLine(screenPosX + drawableScreenW,
                          screenPosY + drawableScreenH,
                          screenPosX + drawableScreenW,
                          screenPosY + drawableScreenH + SIZE_OUTER);

        painter->drawLine(screenPosX + corrW * SCREEN_LINE_WEIGHT + drawableModWidth * (drawableModNumW - 1),
                          screenPosY + drawableScreenH,
                          screenPosX + corrW * SCREEN_LINE_WEIGHT + drawableModWidth * (drawableModNumW - 1),
                          screenPosY + drawableScreenH + SIZE_OUTER);

        painter->drawLine(screenPosX + corrW * SCREEN_LINE_WEIGHT + drawableModWidth * (drawableModNumW - 1),
                          screenPosY + drawableScreenH + SIZE_OUTER * 0.75,
                          screenPosX + drawableScreenW,
                          screenPosY + drawableScreenH + SIZE_OUTER * 0.75);

        drawArrow(painter,
                  screenPosX + corrW * SCREEN_LINE_WEIGHT + drawableModWidth * (drawableModNumW - 1) + ARROW_H,
                  screenPosY + drawableScreenH + SIZE_OUTER * 0.75,
                  screenPosX + corrW * SCREEN_LINE_WEIGHT + drawableModWidth * (drawableModNumW - 1),
                  screenPosY + drawableScreenH + SIZE_OUTER * 0.75);

        drawArrow(painter,
                  screenPosX + drawableScreenW - ARROW_H,
                  screenPosY + drawableScreenH + SIZE_OUTER * 0.75,
                  screenPosX + drawableScreenW,
                  screenPosY + drawableScreenH + SIZE_OUTER * 0.75);

        // text - module width
        painter->drawText(QRect(screenPosX + corrW * SCREEN_LINE_WEIGHT + drawableModWidth * (drawableModNumW - 1),
                                screenPosY + drawableScreenH + SIZE_OUTER * 0.5, drawableModWidth, SIZE_OUTER * 0.25 - 2
                          ),
                          Qt::TextSingleLine | Qt::AlignHCenter | Qt::AlignVCenter,
                          QString("%1").arg(modW));
    }

    if (drawableModNumH > 1)
    {
        // sizes - module vertical
        painter->drawLine(screenPosX + drawableScreenW,
                          screenPosY + drawableScreenH,
                          screenPosX + drawableScreenW + SIZE_OUTER,
                          screenPosY + drawableScreenH);

        painter->drawLine(screenPosX + drawableScreenW,
                          screenPosY + corrH * SCREEN_LINE_WEIGHT / 2 + drawableModHeight * (drawableModNumH - 1),
                          screenPosX + drawableScreenW + SIZE_OUTER,
                          screenPosY + corrH * SCREEN_LINE_WEIGHT / 2 + drawableModHeight * (drawableModNumH - 1));

        painter->drawLine(screenPosX + drawableScreenW + SIZE_OUTER * 0.75,
                          screenPosY + corrH * SCREEN_LINE_WEIGHT / 2 + drawableModHeight * (drawableModNumH - 1),
                          screenPosX + drawableScreenW + SIZE_OUTER * 0.75,
                          screenPosY + drawableScreenH);

        drawArrow(painter,
                  screenPosX + drawableScreenW + SIZE_OUTER * 0.75,
                  screenPosY + corrH * SCREEN_LINE_WEIGHT / 2 + drawableModHeight * (drawableModNumH - 1) + ARROW_H,
                  screenPosX + drawableScreenW + SIZE_OUTER * 0.75,
                  screenPosY + corrH * SCREEN_LINE_WEIGHT / 2 + drawableModHeight * (drawableModNumH - 1));

        drawArrow(painter,
                  screenPosX + drawableScreenW + SIZE_OUTER * 0.75,
                  screenPosY + drawableScreenH - ARROW_H,
                  screenPosX + drawableScreenW + SIZE_OUTER * 0.75,
                  screenPosY + drawableScreenH);

        // text - module height
        str = QString("%1").arg(modH);
        w = fm.width(str);
        drawTextRotated(painter,
                        QPoint(screenPosX + drawableScreenW + SIZE_OUTER * 0.5 - w/2 + 2,
                               screenPosY + corrH * SCREEN_LINE_WEIGHT / 2 + drawableModHeight * (drawableModNumH - 1) + drawableModHeight/2 - w/2),
                        str,
                        270);
    }

    if (showOverlay)
    {
        // overlay bounds
        QBrush overlayBrush(QColor(0xff, 0xff, 0xbf, 240));
        overlayBrush.setStyle(Qt::Dense2Pattern);
        painter->fillRect(overlayPosX, overlayPosY, OVERLAY_W, OVERLAY_H, overlayBrush);

        if (overlayBorder)
        {
            painter->setBrush(Qt::NoBrush);
            painter->drawRect(overlayPosX, overlayPosY, OVERLAY_W, OVERLAY_H);
        }

        // overlay text
        font = painter->font();
        font.setFamily("Helvetica");
        font.setPointSize(12);
        painter->setFont(font);

        QString textTitle = QString("%1 %2").arg(tr("Video screen")).arg(title);
        font = painter->font();
        font.setBold(true);
        painter->setFont(font);

        painter->drawText(QRect(overlayPosX,
                               overlayPosY,
                               OVERLAY_W,
                               OVERLAY_H / 4),
                         Qt::TextSingleLine | Qt::AlignHCenter | Qt::AlignVCenter,
                         textTitle
        );

        QString textDescr = QString("%1: %2 x %3\n%4: %5 x %6 %7\n%8: %9 %10")
                                .arg(tr("Modules num"))
                                .arg(numModulesW)
                                .arg(numModulesH)
                                .arg(tr("Resolution"))
                                .arg(resX)
                                .arg(resY)
                                .arg(tr("px"))
                                .arg(tr("Power average"))
                                .arg(powerAvg)
                                .arg(tr("kW"));

        font = painter->font();
        font.setBold(false);
        painter->setFont(font);

        painter->drawText(QRect(overlayPosX,
                               overlayPosY + OVERLAY_H / 16,
                               OVERLAY_W,
                               3 * OVERLAY_H / 4),
                         Qt::TextWordWrap | Qt::AlignVCenter | Qt::AlignHCenter,
                         textDescr
        );

        QString textLink = QString("www.araneus.ru");
        font = painter->font();
        font.setUnderline(true);
        painter->setFont(font);
        pen.setColor(Qt::blue); painter->setPen(pen);

        painter->drawText(QRect(overlayPosX,
                               overlayPosY + 0.75*OVERLAY_H,
                               OVERLAY_W,
                               OVERLAY_H / 4),
                         Qt::TextSingleLine | Qt::AlignHCenter | Qt::AlignVCenter,
                         textLink
        );
    } // showOverlay
}

void RenderArea::paintEvent(QPaintEvent *event)
{
    UNUSED(event);

    QPainter painter(this);

    // draw image to widget area
    paintImage(&painter);
}

void RenderArea::saveToFile(QString filename)
{
    QImage image(imageWidth, imageHeight, QImage::Format_RGB32);

    QPainter painter;
    painter.begin(&image);
    paintImage(&painter);
    painter.end();

    image.save(filename);
}

QImage rotateImage90(const QImage &src)
{
    QImage dst(src.height(), src.width(), src.format());

    for (int y = 0; y < src.height(); ++y)
    {
        const uint *srcLine = reinterpret_cast< const uint * > (src.scanLine(y));

        for (int x = 0; x < src.width(); ++x)
            dst.setPixel(src.height() - y - 1, x, srcLine[x]);

    }

    return dst;
}

void RenderArea::copyToClipboard()
{
    QImage image(imageWidth, imageHeight, QImage::Format_RGB32);

    QPainter painter;
    painter.begin(&image);
    paintImage(&painter, true);
    painter.end();

    //image = rotateImage90(image);

    QMimeData* mimeData = new QMimeData();

    QByteArray data;
    QBuffer buffer(&data);

    buffer.open(QIODevice::WriteOnly);
    image.save(&buffer, "PNG");
    buffer.close();
    mimeData->setData("PNG", data);

    QClipboard *clipboard = QApplication::clipboard();
    clipboard->setMimeData( mimeData );
}
