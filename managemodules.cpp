#include <QMessageBox>

#include "managemodules.h"
#include "ui_managemodules.h"
#include "mainwindow.h"
#include "managemodulesitem.h"

ManageModules::ManageModules(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ManageModules)
{
    ui->setupUi(this);

    ui->listWidget->setSelectionMode(QAbstractItemView::NoSelection);

    meditor = new ModuleEditor(this);

    modules = &(qobject_cast<MainWindow *>(parentWidget())->modules);
    ledModels = &(qobject_cast<MainWindow *>(parentWidget())->ledModels);
    variables = &(qobject_cast<MainWindow *>(parentWidget())->variables);
}

void ManageModules::showEvent (QShowEvent* event)
{
    UNUSED(event);

    for (int i = 0; i < modules->count(); i++)
    {
        ManageModulesItem *mmi = new ManageModulesItem(this, (Module *)&(modules->at(i)));
        mmi->setLabelText(modules->at(i).name);

        QListWidgetItem *item = new QListWidgetItem();
        item->setSizeHint(QSize(0, 48));
        ui->listWidget->addItem(item);
        ui->listWidget->setItemWidget(item, mmi);

        // link row signals with list slots
        connect(mmi, SIGNAL(btnEditSignal(Module *)), this, SLOT(slotModuleEdit(Module *)));
        connect(mmi, SIGNAL(btnRemoveSignal(Module *)), this, SLOT(slotModuleRemove(Module *)));
    }
}

void ManageModules::hideEvent (QHideEvent* event)
{
    UNUSED(event);

    QList<QListWidgetItem*> items = ui->listWidget->findItems(QString("*"), Qt::MatchWrap | Qt::MatchWildcard);
    QListWidgetItem *item;
    foreach (item, items)
    {
        ManageModulesItem *mmi = qobject_cast<ManageModulesItem *>(ui->listWidget->itemWidget(item));

        // disconnect dynamically created connections (all signals)
        mmi->disconnect();

        delete mmi;
        delete item;
    }

    MainWindow *m = qobject_cast<MainWindow *>(parentWidget());
    m->updateMainFrame();
}

ManageModules::~ManageModules()
{
    delete meditor;
    delete ui;
}

void ManageModules::on_toolButton_clicked()
{
    // append new module to internal list
    Module *mod = new Module;
    mod->name = tr("<new module>");
    mod->width = mod->height = mod->off_price = mod->min_price =
            mod->pixel_step = mod->pixel_type = mod->led_type =
            mod->matrix_height = mod->matrix_width = mod->power_avg =
            mod->power_max = mod->brightness = 0;

    mod->weight = (*variables)["module_weight"].value.toDouble();

    modules->append(*mod);

    // update listwidget
    hideEvent(NULL);
    showEvent(NULL);
}

void ManageModules::slotModuleEdit(Module *mod)
{
    // open module editor window with selected module data
    meditor->setModulePtr(mod);
    meditor->show();

    // update listwidget
    hideEvent(NULL);
    showEvent(NULL);
}

void ManageModules::slotModuleRemove(Module *mod)
{
    // remove module from internal list
    modules->removeOne(*mod);

    // update listwidget
    hideEvent(NULL);
    showEvent(NULL);
}

void ManageModules::updatePriceLabels(QString sign)
{
    // save value sign for module editor
    valueSign = sign;
}
