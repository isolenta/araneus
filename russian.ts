<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="ru_RU">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="14"/>
        <source>Commercial Proposal</source>
        <translation>Коммерческое предложение</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="88"/>
        <source>Document template:</source>
        <translation>Шаблон предложения:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="135"/>
        <source>Save document</source>
        <translation>Сохранить документ</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="164"/>
        <source>Save image</source>
        <translation>Создать изображение</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="199"/>
        <source>Print</source>
        <translation>Печать</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="241"/>
        <source>Variables editor</source>
        <translation>Редактор переменных</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="276"/>
        <source>Modules editor</source>
        <translation>Редактор модулей</translation>
    </message>
    <message>
        <source>Image</source>
        <translation type="obsolete">Изобр</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="413"/>
        <source>Module parameters:</source>
        <translation>Параметры модуля</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="437"/>
        <source>Base module</source>
        <translation>Базовый модуль</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="483"/>
        <location filename="mainwindow.ui" line="1130"/>
        <source>Width (mm)</source>
        <translation>Ширина (мм)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="522"/>
        <location filename="mainwindow.ui" line="1184"/>
        <source>Height (mm)</source>
        <translation>Высота (мм)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="561"/>
        <source>Weight (kg)</source>
        <translation>Вес (кг)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="708"/>
        <source>Brightness (kd)</source>
        <translation>Яркость (Кд)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="747"/>
        <source>Pixel step (mm)</source>
        <translation>Шаг пикселя (мм)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="864"/>
        <source>Power average (kW)</source>
        <translation>Ср. потр. мощность (кВт)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="903"/>
        <source>Power maximum (kW)</source>
        <translation>Макс. потр. мощность (кВт)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1238"/>
        <source>Area (m^2)</source>
        <translation>Площадь (кв.м.)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1347"/>
        <source> x </source>
        <translation>x</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1394"/>
        <source>Resolution (px)</source>
        <translation>Разрешение (пикс.)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1439"/>
        <source>Panels weight (kg)</source>
        <translation>Вес панелей (кг)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1496"/>
        <source>Weight total (kg)</source>
        <translation>Вес общий (кг)</translation>
    </message>
    <message>
        <source>Weight</source>
        <translation type="obsolete">Вес</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="600"/>
        <source>Led type</source>
        <translation>Тип светодиода</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="639"/>
        <source>Pixel type</source>
        <translation>Тип пикселя</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="672"/>
        <source>Led model</source>
        <translation>Марка светодиода</translation>
    </message>
    <message>
        <source>Brightness:</source>
        <translation type="obsolete">Яркость</translation>
    </message>
    <message>
        <source>Pixel step:</source>
        <translation type="obsolete">Шаг пикселя</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="786"/>
        <source>Matrix width (mm)</source>
        <translation>Ширина матрицы (мм)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="825"/>
        <source>Matrix height (mm)</source>
        <translation>Высота матрицы (мм)</translation>
    </message>
    <message>
        <source>Power average</source>
        <translation type="obsolete">Средняя потр. мощность</translation>
    </message>
    <message>
        <source>Power maximum</source>
        <translation type="obsolete">Максимальная потр. мощность</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="942"/>
        <location filename="mainwindow.cpp" line="70"/>
        <source>Price</source>
        <translation>Цена</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="973"/>
        <source>use own</source>
        <translation>задать собственную</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1005"/>
        <source>Enter module name</source>
        <translation>Введите имя модуля</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1015"/>
        <source>Save as new</source>
        <translation>Сохранить как новый</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1100"/>
        <source>Screen parameters:</source>
        <translation>Характеристики экрана</translation>
    </message>
    <message>
        <source>Width (m)</source>
        <translation type="obsolete">Ширина (м)</translation>
    </message>
    <message>
        <source>Height (m)</source>
        <translation type="obsolete">Высота (м)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1677"/>
        <location filename="mainwindow.cpp" line="71"/>
        <source>Metal price</source>
        <translation>Цена в расчете на 1 модуль</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1722"/>
        <location filename="mainwindow.cpp" line="75"/>
        <source>Metal summa</source>
        <translation>Суммарная стоимость м/к</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1879"/>
        <location filename="mainwindow.cpp" line="72"/>
        <source>Video price</source>
        <translation>Цена видеосервера</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1930"/>
        <location filename="mainwindow.cpp" line="73"/>
        <source>Delivery price</source>
        <translation>Цена доставки и установки</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1295"/>
        <source>Num modules</source>
        <translation>Количество модулей</translation>
    </message>
    <message>
        <source>Area</source>
        <translation type="obsolete">Площадь</translation>
    </message>
    <message>
        <source>Resolution</source>
        <translation type="obsolete">Разрешение</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1546"/>
        <source>Power consumption:</source>
        <translation>Потребляемая мощность</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1570"/>
        <source>Average (kW)</source>
        <translation>Средняя (кВт)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1609"/>
        <source>Maximum (kW)</source>
        <translation>Максимальная (кВт)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1647"/>
        <source>Metal framework:</source>
        <translation>Металлоконструкция</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1767"/>
        <source>Weight per module (kg)</source>
        <translation>Вес в расчете на 1 модуль (кг)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1821"/>
        <source>Metal weight (kg)</source>
        <translation>Суммарный вес м/к (кг)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1997"/>
        <location filename="mainwindow.cpp" line="76"/>
        <source>Screen sum</source>
        <translation>Стоимость экрана</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2060"/>
        <location filename="mainwindow.cpp" line="74"/>
        <source>Total price</source>
        <translation>Итоговая цена</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="107"/>
        <location filename="mainwindow.cpp" line="203"/>
        <source>Metal framework price</source>
        <translation>В расчете на 1 модуль</translation>
    </message>
    <message>
        <source>Metal framework weight</source>
        <translation type="obsolete">Суммарно за экран</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="113"/>
        <location filename="mainwindow.cpp" line="206"/>
        <source>Metal framework weight per module</source>
        <translation>Вес металлоконструкции из расчета на 1 модуль</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="119"/>
        <location filename="mainwindow.cpp" line="209"/>
        <source>Module default weight</source>
        <translation>Вес модуля по умолчанию</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="125"/>
        <location filename="mainwindow.cpp" line="212"/>
        <source>Videoserver price</source>
        <translation>Цена видеосервера</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="131"/>
        <location filename="mainwindow.cpp" line="215"/>
        <source>Delivery and installation price</source>
        <translation>Цена доставки и установки</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="137"/>
        <location filename="mainwindow.cpp" line="218"/>
        <source>as is</source>
        <translation>по факту</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="137"/>
        <location filename="mainwindow.cpp" line="218"/>
        <source>Text for delivery&amp;install price</source>
        <translation>Строка для цены доставки и установки</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="143"/>
        <location filename="mainwindow.cpp" line="221"/>
        <source>(interpolated)</source>
        <translation>(интерполированные)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="143"/>
        <location filename="mainwindow.cpp" line="221"/>
        <source>pixel step description</source>
        <translation>описание шага пикселя</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="149"/>
        <location filename="mainwindow.cpp" line="224"/>
        <source>* The technology uses interpolation image forming LEDs included in the physical structure of the neighboring pixels. This allows you to get a better and clearer picture.</source>
        <translation>*Технология интерполяции использует для формирования изображения светодиоды, входящие в структуру соседних физических пикселей. Это позволяет получить более качественное и четкое изображение.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="149"/>
        <location filename="mainwindow.cpp" line="224"/>
        <source>interpolation description</source>
        <translation>описание технологии интерполяции</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="155"/>
        <location filename="mainwindow.cpp" line="227"/>
        <source>View angles (by half power, h/w), degrees</source>
        <translation>Угол обзора (по половинной мощности, гориз./вертик.), градусов</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="155"/>
        <location filename="mainwindow.cpp" line="227"/>
        <source>text for DIP view angles</source>
        <translation>текст для угла обзора DIP-светодиодов</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="161"/>
        <location filename="mainwindow.cpp" line="230"/>
        <source>120/90</source>
        <translation>120/90</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="161"/>
        <location filename="mainwindow.cpp" line="230"/>
        <source>value for DIP view angles</source>
        <translation>значение для угла обзора DIP-светодиодов</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="167"/>
        <location filename="mainwindow.cpp" line="233"/>
        <source>View angles (by half power), degrees</source>
        <translation>Угол обзора (по половинной мощности), градусов</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="167"/>
        <location filename="mainwindow.cpp" line="233"/>
        <source>text for SMD view angles</source>
        <translation>текст для угла обзора SMD-светодиодов</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="173"/>
        <location filename="mainwindow.cpp" line="236"/>
        <source>120/120</source>
        <translation>120/120</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="173"/>
        <location filename="mainwindow.cpp" line="236"/>
        <source>value for SMD view angles</source>
        <translation>значение для угла обзора SMD-светодиодов</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="179"/>
        <location filename="mainwindow.cpp" line="239"/>
        <source>value char</source>
        <translation>Символ валюты</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="185"/>
        <location filename="mainwindow.cpp" line="242"/>
        <source>overlab drawing enable on screen image</source>
        <translation>отображать информацию об экране на изображении</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="191"/>
        <location filename="mainwindow.cpp" line="245"/>
        <source>flask LEDs 5 pcs</source>
        <translation>Колбовые светодиоды 5 шт./пикс.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="191"/>
        <location filename="mainwindow.cpp" line="245"/>
        <source>geometry value for DIP leds</source>
        <translation>значение поля &quot;геометрия&quot; для DIP-светодиодов</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="197"/>
        <location filename="mainwindow.cpp" line="248"/>
        <source>SMD (3 in 1)</source>
        <translation>SMD (3 in 1)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="197"/>
        <location filename="mainwindow.cpp" line="248"/>
        <source>geometry value for SMD leds</source>
        <translation>значение поля &quot;геометрия&quot; для SMD-светодиодов</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="320"/>
        <location filename="mainwindow.cpp" line="480"/>
        <source>Warning</source>
        <translation>Внимание</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="320"/>
        <source>Modules settings empty. You need to create at least one to continue work</source>
        <translation>Отсутствуют сохраненные модули. Вам необходимо создать хотя бы один для работы.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="480"/>
        <source>No template files found in template directory. You should to create at least one</source>
        <translation>Отсутствуют шаблоны документов в каталоге templates. Необходимо создать хотя бы один</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="522"/>
        <source>Save as doc</source>
        <translation>Сохранить документ</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1518"/>
        <source>Wrong screen parameters</source>
        <translation>Некорректные параметры экрана</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1523"/>
        <source>Save image as</source>
        <translation>Сохранить изображение как</translation>
    </message>
    <message>
        <source>Official price</source>
        <translation type="obsolete">Официальная цена</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="608"/>
        <source>DIP</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="609"/>
        <source>SMD</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="612"/>
        <source>default</source>
        <translation>обычный</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="613"/>
        <source>interpolate</source>
        <translation>интерполированный</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="693"/>
        <source>off</source>
        <translation>офиц.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="694"/>
        <source>min</source>
        <translation>мин.</translation>
    </message>
    <message>
        <source>Off:</source>
        <translation type="obsolete">офиц.</translation>
    </message>
    <message>
        <source>Min:</source>
        <translation type="obsolete">мин.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1344"/>
        <location filename="mainwindow.cpp" line="1518"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1344"/>
        <source>Unable to call Microsoft Word to process</source>
        <translation>Невозможно создать экземпляр Microsoft Word</translation>
    </message>
</context>
<context>
    <name>ManageModules</name>
    <message>
        <location filename="managemodules.ui" line="14"/>
        <source>Manage modules</source>
        <translation>Управление модулями</translation>
    </message>
    <message>
        <location filename="managemodules.ui" line="37"/>
        <source>Add</source>
        <translation>Добавить</translation>
    </message>
    <message>
        <location filename="managemodules.cpp" line="74"/>
        <source>&lt;new module&gt;</source>
        <translation>&lt;новый модуль&gt;</translation>
    </message>
</context>
<context>
    <name>ManageModulesItem</name>
    <message>
        <location filename="managemodulesitem.cpp" line="29"/>
        <source>Edit</source>
        <translation>Редактировать</translation>
    </message>
    <message>
        <location filename="managemodulesitem.cpp" line="41"/>
        <source>Remove</source>
        <translation>Удалить</translation>
    </message>
</context>
<context>
    <name>ModuleEditor</name>
    <message>
        <location filename="moduleeditor.ui" line="18"/>
        <source>Module editor</source>
        <translation>Редактор модулей</translation>
    </message>
    <message>
        <location filename="moduleeditor.ui" line="47"/>
        <source>Name</source>
        <translation>Тип модуля</translation>
    </message>
    <message>
        <location filename="moduleeditor.ui" line="92"/>
        <source>Width (mm)</source>
        <translation>Ширина (мм)</translation>
    </message>
    <message>
        <location filename="moduleeditor.ui" line="137"/>
        <source>Height (mm)</source>
        <translation>Высота (мм)</translation>
    </message>
    <message>
        <location filename="moduleeditor.ui" line="182"/>
        <source>Weight (kg)</source>
        <translation>Вес (кг)</translation>
    </message>
    <message>
        <location filename="moduleeditor.ui" line="365"/>
        <source>Brightness (kd)</source>
        <translation>Яркость (Кд)</translation>
    </message>
    <message>
        <location filename="moduleeditor.ui" line="410"/>
        <source>Pixel step (mm)</source>
        <translation>Шаг пикселя (мм)</translation>
    </message>
    <message>
        <location filename="moduleeditor.ui" line="545"/>
        <source>Power average (kW)</source>
        <translation>Ср. потр. мощность (кВт)</translation>
    </message>
    <message>
        <location filename="moduleeditor.ui" line="590"/>
        <source>Power maximum (kW)</source>
        <translation>Макс. потр. мощность (кВт)</translation>
    </message>
    <message>
        <location filename="moduleeditor.ui" line="635"/>
        <location filename="moduleeditor.cpp" line="107"/>
        <source>Official price</source>
        <translation>Официальная цена</translation>
    </message>
    <message>
        <location filename="moduleeditor.ui" line="680"/>
        <location filename="moduleeditor.cpp" line="108"/>
        <source>Minimal price</source>
        <translation>Минимальная цена</translation>
    </message>
    <message>
        <location filename="moduleeditor.ui" line="272"/>
        <source>Pixel type</source>
        <oldsource>Pixel step</oldsource>
        <translation>Тип пикселя</translation>
    </message>
    <message>
        <location filename="moduleeditor.ui" line="227"/>
        <source>Led type</source>
        <translation>Тип светодиода</translation>
    </message>
    <message>
        <location filename="moduleeditor.ui" line="455"/>
        <source>Matrix width (mm)</source>
        <translation>Ширина матрицы (мм)</translation>
    </message>
    <message>
        <location filename="moduleeditor.ui" line="500"/>
        <source>Matrix height (mm)</source>
        <translation>Высота матрицы (мм)</translation>
    </message>
    <message>
        <source>Power average</source>
        <translation type="obsolete">Средняя потр. мощность</translation>
    </message>
    <message>
        <source>Power maximum</source>
        <translation type="obsolete">Макс. потр. мощность</translation>
    </message>
    <message>
        <source>Brightness</source>
        <translation type="obsolete">Яркость</translation>
    </message>
    <message>
        <source>Weight</source>
        <translation type="obsolete">Вес</translation>
    </message>
    <message>
        <location filename="moduleeditor.ui" line="317"/>
        <source>Led model</source>
        <translation>Марка светодиодов</translation>
    </message>
    <message>
        <location filename="moduleeditor.ui" line="733"/>
        <source>OK</source>
        <translation></translation>
    </message>
    <message>
        <location filename="moduleeditor.ui" line="740"/>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
    <message>
        <location filename="moduleeditor.cpp" line="117"/>
        <source>DIP</source>
        <translation></translation>
    </message>
    <message>
        <location filename="moduleeditor.cpp" line="118"/>
        <source>SMD</source>
        <translation></translation>
    </message>
    <message>
        <location filename="moduleeditor.cpp" line="130"/>
        <source>default</source>
        <translation>обычный</translation>
    </message>
    <message>
        <location filename="moduleeditor.cpp" line="131"/>
        <source>interpolate</source>
        <translation>интерполированный</translation>
    </message>
</context>
<context>
    <name>RenderArea</name>
    <message>
        <location filename="renderarea.cpp" line="404"/>
        <source>Video screen</source>
        <translation>Видеоэкран</translation>
    </message>
    <message>
        <location filename="renderarea.cpp" line="418"/>
        <source>Modules num</source>
        <translation>Количество модулей</translation>
    </message>
    <message>
        <location filename="renderarea.cpp" line="421"/>
        <source>Resolution</source>
        <translation>Разрешение</translation>
    </message>
    <message>
        <location filename="renderarea.cpp" line="424"/>
        <source>px</source>
        <translation>пикселей</translation>
    </message>
    <message>
        <location filename="renderarea.cpp" line="425"/>
        <source>Power average</source>
        <translation>Средняя потр. мощность</translation>
    </message>
    <message>
        <location filename="renderarea.cpp" line="427"/>
        <source>kW</source>
        <translation>кВт</translation>
    </message>
</context>
<context>
    <name>ScreenImageForm</name>
    <message>
        <source>Screen image</source>
        <translation type="obsolete">Изображение экрана</translation>
    </message>
</context>
<context>
    <name>VariablesDialog</name>
    <message>
        <location filename="variablesdialog.ui" line="14"/>
        <source>Edit variables</source>
        <translation>Редактор переменных</translation>
    </message>
    <message>
        <location filename="variablesdialog.cpp" line="14"/>
        <source>Name</source>
        <translation>Имя</translation>
    </message>
    <message>
        <location filename="variablesdialog.cpp" line="15"/>
        <source>Value</source>
        <translation>Значение</translation>
    </message>
    <message>
        <location filename="variablesdialog.cpp" line="16"/>
        <source>Description</source>
        <translation>Комментарий</translation>
    </message>
</context>
</TS>
