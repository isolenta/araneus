#ifndef SCREENIMAGEFORM_H
#define SCREENIMAGEFORM_H

#include <QMainWindow>
#include <QString>
#include "renderarea.h"

namespace Ui {
    class ScreenImageForm;
}

class ScreenImageForm : public QMainWindow
{
    Q_OBJECT

public:
    explicit ScreenImageForm(QWidget *parent = 0);
    ~ScreenImageForm();
    void setParams(
            double numModulesW,
            double numModulesH,
            double screenW,
            double screenH,
            double modW,
            double modH,
            QString title,
            int resX,
            int resY,
            double powerAvg
    );

    private slots:
    void on_toolButton_clicked();

    private:
    Ui::ScreenImageForm *ui;
    RenderArea *renderArea;
};

#endif // SCREENIMAGEFORM_H
