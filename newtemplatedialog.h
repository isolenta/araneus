#ifndef NEWTEMPLATEDIALOG_H
#define NEWTEMPLATEDIALOG_H

#include <QDialog>

namespace Ui {
class NewTemplateDialog;
}

class NewTemplateDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit NewTemplateDialog(QWidget *parent = 0);
    ~NewTemplateDialog();
    
private:
    Ui::NewTemplateDialog *ui;
};

#endif // NEWTEMPLATEDIALOG_H
